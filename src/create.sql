-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `clickit_maps`
--

CREATE TABLE IF NOT EXISTS `clickit_maps` (
  `id` int(11) NOT NULL,
  `spawn_x` double NOT NULL DEFAULT '0',
  `spawn_y` double NOT NULL DEFAULT '0',
  `spawn_z` double NOT NULL DEFAULT '0',
  `specspawn_x` double NOT NULL DEFAULT '0',
  `specspawn_y` double NOT NULL DEFAULT '0',
  `specspawn_z` double NOT NULL DEFAULT '0',
  `world` varchar(100) NOT NULL,
  `display_name` varchar(150) NOT NULL,
  `creator` varchar(100) NOT NULL DEFAULT '§cUnknown',
  `link` varchar(200) NOT NULL DEFAULT '§cUnknown',
  `meta` varchar(1000) NOT NULL DEFAULT 'None',
  `listed` varchar(20) NOT NULL DEFAULT 'true',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


