package net.parablack.ClickIt.api;

import java.util.HashMap;

import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.manager.StatAccessManager;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.API.api.SpectatorAPI;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.MiniGame.api.player.IBasicStatistics;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class PluginPlayer implements IPluginPlayer, IBasicStatistics {

	public static HashMap<User, PluginPlayer> fromUser = new HashMap<>();

	private boolean alive = true;
	private Player player;

	// MySQL - Loaded
	private int points;
	private int kills;
	private int deaths;
	private int wins;
	private int gamesPlayed;

	private User user;

	private int clicks = 0;

	private Player lastHitted;

	private Inventory clickInv;

	public PluginPlayer(final User u, boolean alive) {
		this.player = u.getPlayer();
		fromUser.put(u, this);
		this.alive = alive;
		this.user = u;

		this.clickInv = ClickIt.getInstance().getGameManager().masterInv;

		@SuppressWarnings("deprecation")
		ItemStack itm = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.BLACK.getData());
		ItemMeta im = itm.getItemMeta();
		im.setDisplayName(StringManager.i_clickit);
		itm.setItemMeta(im);
		Inventory inv = Bukkit.createInventory(null, 54, StringManager.i_clickit);
		for (int i = 0; i < 54; i++) {
			inv.setItem(i, itm);
		}
		ItemStack itm1 = new ItemStack(Material.ENDER_PORTAL);
		ItemMeta im1 = itm1.getItemMeta();
		im1.setDisplayName(StringManager.i_clickit_prem);
		itm1.setItemMeta(im1);
		Inventory inv1 = Bukkit.createInventory(null, 9, StringManager.i_clickit_prem);
		for (int i = 0; i < 9; i++) {
			inv1.setItem(i, itm1);
		}

		this.clickInv = inv;

		//		ResultSet rs = CurrentSQL.query(
		//				"SELECT * FROM gm_clickit_players WHERE uuid=?", p
		//						.getUniqueId().toString());
		HashMap<String, Integer> currstats = StatAccessManager.get("stats_ci", player.getName(), new String[] { "wins", "kills", "deaths", "points", "gamesplayed" });
		points = currstats.get("points");
		kills = currstats.get("kills");
		wins = currstats.get("wins");
		deaths = currstats.get("deaths");
		gamesPlayed = currstats.get("gamesplayed");

	}

	@Override
	public boolean isSpectating() {
		return !alive;
	}

	public void setSpectator() {
		alive = false;

		Bukkit.getScheduler().scheduleSyncDelayedTask(ClickIt.getInstance(), new Runnable() {

			@Override
			public void run() {

				getPlayer().teleport(ClickIt.getInstance().getMapManager().getPlayedMap().getSpectatorSpawn());
				ClickIt.getInstance().getSpectatorInventory().add(returnThis());
				getPlayer().getInventory().setItem(8, me.kombustorlp.gameapi.Data.backtohub);
				getPlayer().setFallDistance(0);

			}
		}, 3);
		Bukkit.getScheduler().scheduleSyncDelayedTask(ClickIt.getInstance(), new Runnable() {

			@Override
			public void run() {
				getPlayer().setAllowFlight(true);
				getPlayer().setFlying(true);
			}
		}, 5);

	}

	public int getPoints() {
		return points;
	}

	public int getWins() {
		return wins;
	}

	public int getKills() {
		return kills;
	}

	public int getDeaths() {
		return deaths;
	}

	@Override
	public int getGamesPlayed() {
		return gamesPlayed;
	}

	@Override
	public void setKills(int kills) {
		GameAPI.getStatManager().set(user, "kills", kills);
		this.kills = kills;
	}

	@Override
	public void addKill() {
		setKills(getKills() + 1);
	}

	@Override
	public void addPoints(int points) {
		setPoints(getPoints() + points);

		if(getPoints() >= 10000 && !getUser().hasAchievement(Achievements.tenkpoints.getIdentifier())) Achievements.tenkpoints.gainAchievement(getUser().getName());
		if(getPoints() >= 50000 && !getUser().hasAchievement(Achievements.fiftykpoints.getIdentifier())) Achievements.fiftykpoints.gainAchievement(getUser().getName());
		if(getPoints() >= 100000 && !getUser().hasAchievement(Achievements.hundredkpoints.getIdentifier())) Achievements.hundredkpoints.gainAchievement(getUser().getName());

	}

	@Override
	public void setPoints(int points) {
		GameAPI.getStatManager().set(user, "points", kills);
		this.points = points;
	}

	@Override
	public void setDeaths(int deaths) {
		GameAPI.getStatManager().set(user, "deaths", kills);
		this.deaths = deaths;
	}

	@Override
	public void addDeath() {
		setDeaths(getDeaths() + 1);
	}

	@Override
	public void setWins(int wins) {
		GameAPI.getStatManager().set(user, "wins", kills);
		this.wins = wins;
	}

	@Override
	public void addWin() {
		setWins(getWins() + 1);
	}

	@Override
	public void setGamesPlayed(int gamesplayed) {
		GameAPI.getStatManager().set(user, "gamesplayed", kills);
		this.gamesPlayed = gamesplayed;
	}

	@Override
	public void addGamePlayed() {
		setGamesPlayed(getGamesPlayed() + 1);
	}

	public int getClicks() {
		return clicks;
	}

	public void addClicks(int clicks) {
		this.clicks += clicks;
	}

	public void setClicks(int clicks) {
		this.clicks = clicks;
	}

	public void die() {
		alive = false;
		SpectatorAPI.getAlive().remove(this);
		SpectatorAPI.getSpectators().add(this);
	}

	public Inventory getClickInventory() {
		return clickInv;
	}

	public void setLastHitted(Player lastHitted) {
		this.lastHitted = lastHitted;
	}

	public Player getLastHitted() {
		return lastHitted;
	}

	public void hasKilled(Player p) {
		addPoints(20);
		addKill();
		getUser().addCoins(10);
		Message.p_Earn_Points_kill.send(getPlayer(), p.getDisplayName(), getPoints() + "");
		// parablackAPI.getPointManager().earnPoints(winner.getName(), 10);
		if(getPlayer().getItemInHand().getType() == Material.COMPASS) Achievements.trackerpower.gainAchievement(player.getName());;
		if(getKills() == 1) Achievements.firstkill.gainAchievement(player.getName());;
		if(getKills() == 100) Achievements.hundredkills.gainAchievement(player.getName());
	}

	@Override
	public Player getPlayer() {
		return player;
	}

	private IPluginPlayer returnThis() {
		return this;
	}

	public void destroy() {
		fromUser.remove(getUser());
		SpectatorAPI.unregister(this);

	}

	public User getUser() {
		return user;
	}

}
