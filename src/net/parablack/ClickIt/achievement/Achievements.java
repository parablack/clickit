package net.parablack.ClickIt.achievement;

import me.kombustorlp.gameapi.achievements.Achievement;
import me.kombustorlp.gameapi.achievements.AchievementManager;

public class Achievements {

	public static Achievement firstkill = new Achievement("ci_firstkill", "I got you!", new String[]{"Make your first kill"}, 20);
	public static Achievement firstwin = new Achievement("ci_firstwin", "The best around", new String[]{"Win your first round"}, 50);
	public static Achievement hundredclicks = new Achievement("ci_100clicks",	 "Master Clicker",new String[]{ "Collect 100 clicks"}, 50);
	public static Achievement fisher = new Achievement("ci_fisher", "Fisher", new String[]{"Catch a fish"}, 10);
	public static Achievement lobbyclicker = new Achievement("ci_lobbyclicker", "Lobby Clicker", new String[]{"Open your click inventory", "in the lobby with /inv"}, 10);
	public static Achievement trackerpower = new Achievement("ci_trackerpower", "Tracker Power", new String[]{"Kill a player with", "your tracker!"},  50);
	public static Achievement kingwins = new Achievement("ci_kingwins", "King Wins", new String[]{"Win a game with", "an equipped golden helmet"},  20);
	public static Achievement powerbow = new Achievement("ci_powerbow", "Power Bow", new String[]{"Enchant a bow with", "Power 1!"},  30);
	public static Achievement remis = new Achievement("ci_remis", "Remis", new String[]{"Be in a round with no winner"}, 30);
	public static Achievement luckyoff = new Achievement("ci_luckyoff", "Lucky Offensive", new String[]{"Uncover your iron sword", "with your last click!"}, 80);
	public static Achievement affectthree = new Achievement("ci_affect3", "Effects of death", new String[]{"Affect 3 players with blindness", "with 1 bone"}, 25);
	public static Achievement diefire = new Achievement("ci_diefire", "That hurts", new String[]{"Die by fire"}, 30);
	public static Achievement goodgame = new Achievement("ci_goodgame", "Good Game", new String[]{"Write 'gg' in the chat"}, 10);
	public static Achievement spectateyt = new Achievement("ci_specyt", "Give me fame", new String[]{"Spectate a youtuber"}, 10);
	public static Achievement hundredkills = new Achievement("ci_kill100", "Master Killer", new String[]{"Kill 100 players"}, 75);
	public static Achievement diehundred = new Achievement("ci_die100", "Not again", new String[]{"Die 100 times"}, 50);
	public static Achievement winten = new Achievement("ci_win10", "Pro like", new String[]{"Win ten rounds"}, 30);
	public static Achievement winhundred = new Achievement("ci_win100", "Look up to me", new String[]{"Win 100 rounds"}, 50);
	public static Achievement tenkpoints = new Achievement("ci_10kpoints", "Point farmer", new String[]{"Earn 10000 points"}, 50);
	public static Achievement fiftykpoints = new Achievement("ci_50kpoints", "Addicted", new String[]{"Earn 50000 points"}, 75);
	public static Achievement hundredkpoints = new Achievement("ci_100kpoints", "Cheater", new String[]{"Earn 100000 points"}, 100);

	public static void init(){
		AchievementManager.addAchievement(firstkill);
		AchievementManager.addAchievement(firstwin);
		AchievementManager.addAchievement(hundredclicks);
		AchievementManager.addAchievement(fisher);
		AchievementManager.addAchievement(lobbyclicker);
		AchievementManager.addAchievement(trackerpower);
		AchievementManager.addAchievement(kingwins);
		AchievementManager.addAchievement(powerbow);
		AchievementManager.addAchievement(remis);
		AchievementManager.addAchievement(luckyoff);
		AchievementManager.addAchievement(affectthree);
		AchievementManager.addAchievement(diefire);
		AchievementManager.addAchievement(goodgame);
		AchievementManager.addAchievement(spectateyt);
		AchievementManager.addAchievement(hundredkills);
		AchievementManager.addAchievement(diehundred);
		AchievementManager.addAchievement(winten);
		AchievementManager.addAchievement(winhundred);
		AchievementManager.addAchievement(fiftykpoints);
		AchievementManager.addAchievement(tenkpoints);
		AchievementManager.addAchievement(hundredkpoints);
		for(Achievement achiev : AchievementManager.getAchievements())
			System.out.println("Registered achievement: "+achiev.getDisplayName()+"|"+achiev.getIdentifier());
	}
}
