package net.parablack.ClickIt.exception;

public class PlayerDeadException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PlayerDeadException(String s){
		super(s);
	}

	public PlayerDeadException() {
	}

}
