package net.parablack.ClickIt.server;

public class UpdateTracker implements Runnable {

	@Override
	public void run() {
		while (true) {
			for (PlayerTrack pt : PlayerTrack.allTracks) {

				try {
					pt.refresh();
				} catch (Exception e) {
					System.out.println("***** Exception in Task #UpdateTracks: " + e.getMessage() + ". Skipping & closing!");
					pt.close();
				}
			}

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
