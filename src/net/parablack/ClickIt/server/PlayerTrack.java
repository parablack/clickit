package net.parablack.ClickIt.server;

import java.util.ArrayList;

import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.exception.PlayerDeadException;
import net.parablack.ClickIt.utils.SC;

import org.bukkit.entity.Player;

public class PlayerTrack {
	public static ArrayList<PlayerTrack> allTracks = new ArrayList<>();
	
	private Player tracker;
	private Player tracked;
	
	public PlayerTrack(Player tracker, Player tracked) {
		this.setTracker(tracker);
		this.setTracked(tracked);
		allTracks.add(this);
	}

	public Player getTracked() {
		return tracked;
	}

	public void setTracked(Player tracked) {
		this.tracked = tracked;
	}

	public Player getTracker() {
		return tracker;
	}

	public void setTracker(Player tracker) {
		this.tracker = tracker;
	}
	public void refresh() throws NullPointerException, PlayerDeadException{
		
		if(tracker == null) throw null;
		if(tracked == null) throw null;
		if(!tracker.canSee(tracked)) throw new PlayerDeadException();
		 PluginPlayer pl1 = SC.from(tracker);
		 PluginPlayer pl2 = SC.from(tracked);
		
		
		if(pl1.isSpectating() || pl2.isSpectating()) throw new PlayerDeadException();
		
		tracker.setCompassTarget(tracked.getLocation());
		
	}
	
	public void close(){
		allTracks.remove(this);
	}
}
