package net.parablack.ClickIt.server;

import java.util.Timer;
import java.util.TimerTask;

import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.Message;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ServerManager {
	public static String host = "//";
	public static final String license = "3129ct97";
	
	public static void enable(){
		
		
		System.out.println("Enabling ClickIt v. 2.0.1 by parablack (& KombustorLP) with license: "+ ServerManager.license);
		System.out.println("ClickIt: Creating SQL Tables...");
		
		Bukkit.getMessenger().registerOutgoingPluginChannel(ClickIt.getInstance(), "BungeeCord");
		Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "gamerule keepInventory false");
//		System.out.println(" + + + save-off + + +");
		Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "save-off");
		Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "gamerule doTileDrops false");
	}
	public static String getLicense(){ return license; }
	public static String getHost(){ return host; }
	public static void restartServer(){
		Message.m_Restart_Pre.broadcast();
		restartServerRawDelayed(15000);
	}

	public static void kickPlayer(final Player p, final String msg){
		Bukkit.getScheduler().runTask(ClickIt.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				p.kickPlayer(msg);
				
			}
		}); 
	}
	public static void restartServerRawDelayed(int delay){
		
		Timer t = new Timer();
		t.schedule(new TimerTask() {
			@Override
			public void run() {
				ClickIt.getInstance().getGameManager().setGameState(GameState.RESTART);
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
			}
		}, delay);
	}
}
