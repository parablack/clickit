package net.parablack.ClickIt.game;

import me.kombustorlp.gameapi.servermanager.ServerState;
import me.kombustorlp.gameapi.servermanager.StateValue;
import net.parablack.API.api.SpectatorAPI;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.ClickIt.utils.ClickItConstants;
import net.parablack.ClickIt.utils.SC;
import net.parablack.ClickIt.utils.StateType;
import net.parablack.MiniGame.api.game.Countdown;
import net.parablack.MiniGame.api.game.IGameManager.GameState;
import net.parablack.MiniGame.api.map.IMap;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;


public class Lobby extends Countdown implements StateType {


	public Lobby() {
		super(ClickItConstants.T_LOBBY_DURATION);
	}


	@SuppressWarnings("deprecation")
	@Override
	public void next(int leftTime) {
		
		if(leftTime == 10) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.getInventory().clear();
				p.getInventory().setArmorContents(null);
			}
			ClickIt.getInstance().getMapManager().choose();
			IMap played = ClickIt.getInstance().getMapManager().getPlayedMap();
			Bukkit.getServer().createWorld(new WorldCreator(played.getWorldName()));
			//ClickIt.getInstance().getPluginLogger().logAdmin("Loaded world " + played.getWorldName());
			World w = Bukkit.getWorld(played.getWorldName());
			
			w.setDifficulty(Difficulty.PEACEFUL);
			w.setDifficulty(Difficulty.EASY);
			w.setAutoSave(false);
			w.setGameRuleValue("doMobSpawning", "false");
			w.setGameRuleValue("keepInventory", "false");
			w.setTime(2000);
			
			Message.m_Map_will_play.broadcast(ClickIt.getInstance().getMapManager().getPlayedMap().getName());
		}
		if(leftTime % 20 == 0 || leftTime <= 10) {
			Message.m_Tl_Lobby.broadcast(leftTime + "");
		}
		if(leftTime ==60){
			Message.m_Maps_Vote.broadcast();
			for(int i = 0; i < ClickIt.getInstance().getMapManager().getVotableMaps().size(); i++){
				IMap map = ClickIt.getInstance().getMapManager().getVotableMaps().get(i);
				Bukkit.broadcastMessage(StringManager.prefix + "�6" + i + ": �9" + map.getName() + "�6 (�b" + map.getVotes() + "�6).");
			}
		}
		if(leftTime == 5){
			ClickIt.getPluginServer().set(StateValue.SERVERSTATE, ServerState.NOTJOINABLE.getDatabaseName());
		}
		if(leftTime == 11) {
			if(Bukkit.getOnlinePlayers().length < ClickItConstants.T_PLAYERS_NEEDED) {
				Message.e_Not_Enough_Players.broadcast();
				super.setTimeLeft(ClickItConstants.T_LOBBY_DURATION);
			}
		}
		
	}

	@SuppressWarnings("deprecation")
	@Override
	public void end() {
	
		for(Player p : Bukkit.getOnlinePlayers()){
			
			SpectatorAPI.getAlive().add(SC.from(p));
			
		}
		
		new Warmup().start();

		
		
		
	}

	@Override
	public void initialize() {
		ClickIt.getInstance().getMapManager().getLobby().getSpawn().getWorld().setTime(2000);
	//	GameManager.checkItems();	Whats that???
	}


	@Override
	public GameState getGameState() {
		return GameState.LOBBY;
	}

}
