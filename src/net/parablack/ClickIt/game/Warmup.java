package net.parablack.ClickIt.game;

import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.ClickIt.utils.StateType;
import net.parablack.MiniGame.api.game.Countdown;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class Warmup extends Countdown implements StateType{

	public static ItemStack chest;
	
	public Warmup() {
		super(180);
	}


	public static void teleportspawn() {
		chest = new ItemStack(Material.CHEST);
		ItemMeta im = chest.getItemMeta();
		im.setDisplayName(StringManager.i_Chest);
		chest.setItemMeta(im);
		

		Bukkit.getScheduler().runTask(ClickIt.getInstance(), new Runnable() {

			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					p.getInventory().clear();
					p.teleport(ClickIt.getInstance().getMapManager().getPlayedMap().getSpawn());
					p.getInventory().addItem(chest);
				}
			}
		});

	}

	@Override
	public void next(int leftTime) {
		if (leftTime == 140) {
			Bukkit.getServer().broadcastMessage(String.format(StringManager.m_Map_Map, ClickIt.getInstance().getMapManager().getPlayedMap().getName()));
			Bukkit.getServer().broadcastMessage(String.format(StringManager.m_Map_Creator, ClickIt.getInstance().getMapManager().getPlayedMap().getCreator()));
			Bukkit.getServer().broadcastMessage(String.format(StringManager.m_Map_Link, ClickIt.getInstance().getMapManager().getPlayedMap().getLink()));
		}
		if (leftTime % 20 == 0 || leftTime < 11) {
			Message.m_Tl_Warmup.broadcast(leftTime + "");
		}
	}

	@Override
	public void end() {
		Message.m_Grace_Over.broadcast();
		new Ingame().start();
	}

	@Override
	public void initialize() {
		ClickIt.getInstance().getMapManager().getPlayedMap().getSpawn().getWorld().setTime(0);
		teleportspawn();
		
	}

	@Override
	public GameState getGameState() {
		return GameState.WARMUP;
	}

}
