package net.parablack.ClickIt.game;

import java.util.ArrayList;
import java.util.HashMap;

import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.StringManager;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryManager {
	public enum MasterItems{
		BLANC, LIGHTNING, GOLD_NUGGET, FLINT, STRING, MAGMA_CREAM, SLIME_BALL, FEATHER, NETHER_WART, MASTER_ITEM;
	}
	public static HashMap<MasterItems, ItemStack> getItems = new HashMap<>();

	public static void createMasterInv() {
		
		{
		setTheText();	

		Inventory inv = Bukkit.createInventory(null, 54);
		ArrayList<Integer> zahlen = new ArrayList<>(); 
		for(int i=0; i<54 ; i++){
			zahlen.add(i);
		}
		{ItemStack im = new ItemStack(Material.WOOD_SWORD);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.FISHING_ROD);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.GOLD_NUGGET);;int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.MAGMA_CREAM);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.NETHER_WART);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.STRING);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.LEATHER_LEGGINGS);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.LEATHER_CHESTPLATE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.COOKED_FISH, 4);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.CHAINMAIL_BOOTS);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.CAKE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.CHAINMAIL_CHESTPLATE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.GOLD_HELMET);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.IRON_BOOTS);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.IRON_LEGGINGS);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.IRON_CHESTPLATE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.CAKE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.DIAMOND_BOOTS);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.DIAMOND_HELMET);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.DIAMOND_LEGGINGS);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.DIAMOND_CHESTPLATE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.GOLD_SWORD);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.IRON_SWORD);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.STONE_SWORD);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.IRON_AXE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.WOOD_SWORD);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.BOW);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.LEATHER_BOOTS);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.COOKED_BEEF, 2);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.GOLDEN_APPLE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.CAKE);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.FEATHER);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.BOW);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.ARROW, 12);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.SLIME_BALL);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.LIGHTNING);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.BAKED_POTATO, 3);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.LIGHTNING);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.CARROT_ITEM);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.COOKIE, 3);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.FLINT);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.ARROW, 12);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.POTION, 1, (short) 16386);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = new ItemStack(Material.FISHING_ROD);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
		{ItemStack im = getItems.get(MasterItems.BLANC);int rdm= (int) (Math.random()*zahlen.size());int slot = zahlen.get(rdm);inv.setItem(slot, im);zahlen.remove(rdm);}
	
		
		ClickIt.getInstance().getGameManager().masterInv = inv;
		}
			
	}

	@SuppressWarnings("deprecation")
	private static void setTheText() {
		{ItemStack is = new ItemStack(Material.BLAZE_ROD);ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_Lohenrute);is.setItemMeta(im);getItems.put(MasterItems.LIGHTNING, is);}
		{ItemStack is = new ItemStack(Material.GOLD_NUGGET);ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_GoldNugget);is.setItemMeta(im);getItems.put(MasterItems.GOLD_NUGGET, is);}
		{ItemStack is = new ItemStack(Material.FLINT);ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_Flint);is.setItemMeta(im);getItems.put(MasterItems.FLINT, is);}
		{ItemStack is = new ItemStack(Material.STRING);ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_String);is.setItemMeta(im);getItems.put(MasterItems.STRING, is);}
		{ItemStack is = new ItemStack(Material.MAGMA_CREAM);ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_MagmaCream);is.setItemMeta(im);getItems.put(MasterItems.MAGMA_CREAM, is);}
		{ItemStack is = new ItemStack(Material.SLIME_BALL);ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_SlimeBall);is.setItemMeta(im);getItems.put(MasterItems.SLIME_BALL, is);}
		{ItemStack is = new ItemStack(Material.FEATHER);ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_Feather);is.setItemMeta(im);getItems.put(MasterItems.FEATHER, is);}
		{ItemStack is = new ItemStack(Material.BONE);ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_Bone);is.setItemMeta(im);getItems.put(MasterItems.NETHER_WART, is);}
		{ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getData());ItemMeta im = is.getItemMeta();im.setDisplayName(StringManager.st_Niete);is.setItemMeta(im);getItems.put(MasterItems.BLANC, is);}
		
		
	}
}
