package net.parablack.ClickIt.game;

import net.parablack.API.api.SpectatorAPI;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.server.ServerManager;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.ClickIt.utils.StateType;
import net.parablack.MiniGame.api.game.Countdown;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class Ingame extends Countdown implements StateType{

	public Ingame() {
		super(900);
	}



	@SuppressWarnings("deprecation")
	@Override
	public void initialize() {
		ClickIt.getInstance().getGameManager().checkItemsFirst();

		for (Player p : Bukkit.getOnlinePlayers()) {
			p.setFoodLevel(20);
			p.setSaturation(6);
		}
	
	}

	@Override
	public void next(int leftTime) {
		if (leftTime % 120 == 0 || leftTime == 60) {
			Message.m_Tl_Ingame.broadcast(leftTime / 60);
		}
	}

	@Override
	public void end() {
		for(PluginPlayer p : SpectatorAPI.getAlive()) Achievements.remis.gainAchievement(p.getUser().getName());
		Bukkit.broadcastMessage(StringManager.m_Won_Pre);
		Message.m_no_winner.broadcast();
		ServerManager.restartServer();
	}

	@Override
	public GameState getGameState() {
		return GameState.INGAME;
	}



}
