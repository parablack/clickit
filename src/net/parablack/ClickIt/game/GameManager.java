package net.parablack.ClickIt.game;

import me.kombustorlp.gameapi.servermanager.StateValue;
import net.parablack.API.api.SpectatorAPI;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.server.ServerManager;
import net.parablack.ClickIt.server.UpdateTracker;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.ClickIt.utils.ItemUtils;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.Countdown;
import net.parablack.MiniGame.api.game.IGameManager;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class GameManager implements IGameManager{

	private GameState gs = GameState.INIT;
	private Countdown actual;
	
	
	public static void startGame() {
		new Thread(new UpdateTracker()).start();
		new Lobby().start();
	}

	public GameState getGameState() {
		return gs;
	}

	public void checkWinner() {
		
		if (SpectatorAPI.getAlive().size() == 1) {
			try{
			Bukkit.broadcastMessage(StringManager.m_Won_Pre);
			final Player winner = SpectatorAPI.getAlive().get(0).getPlayer();
			PluginPlayer pl = SC.from(winner);
			Message.m_has_won.broadcast(winner.getDisplayName());
			
			Message.p_Won.send(winner);
			pl.addWin();
		//	parablackAPI.getPointManager().earnPoints(highestplayer.getName(), 50);
			pl.addPoints(200);
			
			if (pl.getWins() == 1) Achievements.firstwin.gainAchievement(pl.getUser().getName());
			if (pl.getWins() == 10) Achievements.winten.gainAchievement(pl.getUser().getName());
			if (pl.getWins() == 100) Achievements.winhundred.gainAchievement(pl.getUser().getName());
			if(winner.getInventory().getHelmet() != null) if(winner.getInventory().getHelmet().getType() == Material.GOLD_HELMET) Achievements.kingwins.gainAchievement(pl.getUser().getName());
			
			pl.getUser().addCoins(50);
			
			getActualCountdown().stop();
			
			}catch(Exception e){}
			setGameState(GameState.AFTER_GAME);
			ServerManager.restartServer();
		}
		if (SpectatorAPI.getAlive().size() <= 0) {
			Message.e_Server_Error.broadcast();
			setGameState(GameState.AFTER_GAME);
			ServerManager.restartServer();
		}

	}

	public void setGameState(GameState gs) 
	{
		this.gs = gs;
		ClickIt.getPluginServer().set(StateValue.GAMESTATE, gs.toString());
	}

	public void checkItems() {
		if (SpectatorAPI.getAlive().size() == 4) {
			Message.m_now_compass.broadcast();
			ItemStack tracker = ItemUtils.getTracker();
			for (IPluginPlayer alives : SpectatorAPI.getAlive()) {
				alives.getPlayer().playSound(alives.getPlayer().getLocation(), Sound.ARROW_HIT, 10, 1);
				alives.getPlayer().getInventory().addItem(tracker);
			}
		}
		if (SpectatorAPI.getAlive().size() == 2) {
			Message.m_nowreduced_health.broadcast();
			for (IPluginPlayer alives : SpectatorAPI.getAlive()) {
				alives.getPlayer().playSound(alives.getPlayer().getLocation(), Sound.ARROW_HIT, 10, 1);
				alives.getPlayer().setMaxHealth(10.0);
			}
		}
	}

	public void checkItemsFirst() {
		if (SpectatorAPI.getAlive().size() <= 4) {
			Message.m_now_compass.broadcast();
			ItemStack tracker = ItemUtils.getTracker();
			for (IPluginPlayer alives : SpectatorAPI.getAlive()) {
				alives.getPlayer().getInventory().addItem(tracker);
			}
		}
		if (SpectatorAPI.getAlive().size() <= 2) {
			Message.m_nowreduced_health.broadcast();
			for (IPluginPlayer alives : SpectatorAPI.getAlive()) {
				alives.getPlayer().setMaxHealth(10.0);
			}
		}
	}

//	public static ArrayList<Player> spectator = new ArrayList<>();
//	public static ArrayList<Player> alive = new ArrayList<>();
//	public static HashMap<Player, Inventory> inventory = new HashMap<>();
//	public static HashMap<Player, Inventory> premiuminventory = new HashMap<>();
//	public static HashMap<Player, Integer> playerclicks = new HashMap<>();
	
	public Inventory masterInv;

	@Override
	public void setActualCountdown(Countdown countdown) {
		this.actual = countdown;
	}

	@Override
	public Countdown getActualCountdown() {
		return actual;
	}
	
	public Inventory getMasterInv() {
		return masterInv;
	}
}
