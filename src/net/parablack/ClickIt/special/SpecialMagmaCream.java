package net.parablack.ClickIt.special;

import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class SpecialMagmaCream extends SpecialItem implements Listener {
	public SpecialMagmaCream() {
		super(Material.MAGMA_CREAM);
	}


	@Override
	public void fire(Player p) {
		Message.s_MagmaCream.send(p);
		p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 140, 1));
	}
}
