package net.parablack.ClickIt.special;

import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public class SpecialFlint extends SpecialItem implements Listener {
	public SpecialFlint() {
		super(Material.FLINT);
	}

	@Override
	public void fire(Player p) {

		int affPl = effectNearbyPlayers(p, 5, new PlayerAffection() {

			@Override
			public void onAffect(PluginPlayer p) {
				p.getPlayer().setFireTicks(120);
			}
		});

		Message.s_Flint.send(p, affPl);

	}

}
