package net.parablack.ClickIt.special;

import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class SpecialFeather extends SpecialItem {
	public SpecialFeather() {
		super(Material.FEATHER);
	}

	@Override
	public void fire(Player p) {

		int affPl = effectNearbyPlayers(p, 5, new PlayerAffection() {

			@Override
			public void onAffect(PluginPlayer p) {
				p.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.POISON, 220, 0));
			}
		});

		Message.s_Feather.send(p, affPl);

	}
}
