package net.parablack.ClickIt.special;

import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class SpecialBone extends SpecialItem implements Listener {

	public SpecialBone() {
		super(Material.BONE);
	}

	@Override
	public void fire(final Player p) {


				int affPl = effectNearbyPlayers(p, 6, new PlayerAffection() {

					@Override
					public void onAffect(PluginPlayer p) {
						p.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 0));
					}
				});

				Message.s_Bone.send(p, affPl);
				if(affPl >= 3) Achievements.affectthree.gainAchievement(p.getName());;
	}
}
