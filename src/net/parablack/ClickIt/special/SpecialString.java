package net.parablack.ClickIt.special;

import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class SpecialString extends SpecialItem implements Listener {

	public SpecialString() {
		super(Material.STRING);
	}

	@Override
	public void fire(Player p) {

		int affPl = effectNearbyPlayers(p, 7, new PlayerAffection() {

			@Override
			public void onAffect(PluginPlayer p) {
				p.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 1));
			}
		});

		Message.s_String.send(p, affPl);

	}
}