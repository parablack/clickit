package net.parablack.ClickIt.special;

import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class SpecialSlimeBall extends SpecialItem implements Listener {

	public SpecialSlimeBall() {
		super(Material.SLIME_BALL);
	}

	@Override
	public void fire(Player p) {

		int aff = effectNearbyPlayers(p, 8, new PlayerAffection() {

			@Override
			public void onAffect(PluginPlayer p) {

				p.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 300, 1));
			}
		});
		Message.s_SlimeBall.send(p, aff);

	}
}
