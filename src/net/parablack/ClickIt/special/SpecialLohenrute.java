package net.parablack.ClickIt.special;

import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public class SpecialLohenrute extends SpecialItem implements Listener {
	public SpecialLohenrute() {
		super(Material.BLAZE_ROD);
	}

	@Override
	public void fire(Player p) {
		Message.s_Lohenrute.send(p);
		@SuppressWarnings("deprecation")
		Block b = p.getTargetBlock(null, 50);
		b.getWorld().strikeLightning(b.getLocation());
	}

}
