package net.parablack.ClickIt.special;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;


public abstract class SpecialItem extends GameEvent<PlayerInteractEvent> {
	public static boolean ENABLED = false;
	
	public static void sendUnenabled(Player p) {
		Message.s_unenabled.send(p);

	}

	private Material material;

	public SpecialItem(Material m) {
		this.setMaterial(m);
	}

	public Material getMaterial() {
		return material;
	}

	private void setMaterial(Material m) {
		this.material = m;
	}

	private void remove(Player p) {
		for (int count = 0; count < p.getInventory().getSize(); count++) {
			ItemStack i = p.getInventory().getItem(count);

			if(i == null || i.getType() != this.material) {
				continue;
			}
			if(i.getType() == this.material) {
				if(i.getAmount() == 1) {
					p.getInventory().remove(this.material);
				}
				if(i.getAmount() > 1) {
					i.setAmount(i.getAmount() - 1);
				}
				break;
			}
		}

	}

	@EventHandler
	@Override
	public void onEvent(PlayerInteractEvent e) {
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK && e.getAction() != Action.RIGHT_CLICK_AIR) return;
		if(getMaterial() != e.getMaterial()) return;
		if(SC.gameState() == GameState.LOBBY) return;
		if(SC.from(e.getPlayer()).isSpectating()) return;
		if(SC.gameState() != GameState.INGAME && SC.gameState() != GameState.AFTER_GAME) {
			sendUnenabled(e.getPlayer());
			return;
		}

		fire(e.getPlayer());

		remove(e.getPlayer());
	}

	public abstract void fire(Player p);

	public int effectNearbyPlayers(Player thrower, int range, PlayerAffection aff) {
		int affected = 0;
		for (Entity ent : thrower.getNearbyEntities(range, range, range)) {
			if(ent instanceof Player) {

				PluginPlayer pl = SC.from((Player) ent);
				if(!pl.getPlayer().getName().equals(thrower.getName())) {
					if(!pl.isSpectating()) {
						aff.onAffect(pl);
						affected++;
					}
				}
			}
		}
		return affected;
	}

	public interface PlayerAffection {

		public void onAffect(PluginPlayer p);
	}

}
