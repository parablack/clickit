package net.parablack.ClickIt.special;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SpecialGoldNugget extends SpecialItem implements Listener {
	public SpecialGoldNugget() {
		super(Material.GOLD_NUGGET);
	}

	@Override
	public void fire(Player p) {
		p.removePotionEffect(PotionEffectType.ABSORPTION);
		p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 120, 0));
		
	}

	
	
}
