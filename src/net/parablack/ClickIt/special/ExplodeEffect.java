package net.parablack.ClickIt.special;

import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

public interface ExplodeEffect {
	public void onExplode(Item i, Player fired);
}
