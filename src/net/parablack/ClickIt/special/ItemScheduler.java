package net.parablack.ClickIt.special;

import net.parablack.ClickIt.ClickIt;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;


public class ItemScheduler {

	public static void scheduleExplosion(final Item i, final Player p, long ticks, final ExplodeEffect e) {

		Bukkit.getScheduler().scheduleSyncDelayedTask(ClickIt.getInstance(), new Runnable() {

			@Override
			public void run() {

				e.onExplode(i, p);

				i.remove();

			}
		}, ticks);

	}

	public static Item throwItem(Player p) {
		ItemStack stack = new ItemStack(p.getItemInHand().getType());
		Item i = p.getWorld().dropItem(p.getEyeLocation(), stack);
		i.setPickupDelay(Integer.MAX_VALUE);
		i.setVelocity(p.getLocation().getDirection().multiply(1.2D));
		return i;
	}

	public static void explodeFirework(Item i, Color[] c) {

		Firework fw = (Firework) i.getWorld().spawnEntity(i.getLocation(), EntityType.FIREWORK);

		FireworkMeta fm = fw.getFireworkMeta();

		FireworkEffect feff = FireworkEffect.builder().flicker(true).trail(false).with(Type.BALL_LARGE).withColor(c).build();
		fm.addEffect(feff);
		fw.setFireworkMeta(fm);
		fw.detonate();
	}
	
	
}
