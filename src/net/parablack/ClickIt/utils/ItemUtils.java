package net.parablack.ClickIt.utils;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class ItemUtils {
	public static ItemStack getTracker(){
		return setLore(new ItemStack(Material.COMPASS), "�bTracker", "�eTrack nearby players!");
	}

	public static ItemStack setLore(ItemStack i, String Display, String... lore) {
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(Display);

		im.setLore(Arrays.asList(lore));
		i.setItemMeta(im);
		return i;
	}
}
