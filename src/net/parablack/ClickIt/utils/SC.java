package net.parablack.ClickIt.utils;

import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.entity.Player;
/**
 * Shortcut-Class containing useful methods which can be accessed in an easy way
 * @author parablack
 *
 */
public class SC {
	public static GameState gameState(){
		return ClickIt.getInstance().getGameManager().getGameState();
	}
	
	public static User getUser(Player p){
		return GameAPI.getUser(p);
	}
	
	public static PluginPlayer from(Player p){
		
		if(!PluginPlayer.fromUser.containsKey(getUser(p))){
			new PluginPlayer(getUser(p), true);
		}
		
		return PluginPlayer.fromUser.get(getUser(p));
	}
	
}
