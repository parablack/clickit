package net.parablack.ClickIt.utils;

import org.bukkit.Material;

public class ClickItConstants {
	public static final int T_LOBBY_DURATION = 100;
	public static final int T_WARMUP_DURATION = 180;
	public static final int T_INGAME_DURATION = 900;
	public static final int T_PLAYERS_NEEDED = 6;
	
	public static final Material T_COMPASS_MATERIAL = Material.ENDER_PEARL;
}
