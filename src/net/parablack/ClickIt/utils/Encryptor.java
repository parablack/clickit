package net.parablack.ClickIt.utils;

public class Encryptor {
	public static String encrypt(String s) { 
	  	  
        char[] base64Alphabet = new char[] {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/', '='
        };

        int length = s.length();
        for( ; ((length * 8) % 3) != 0; length++);
        boolean[] msg = new boolean[length * 8];
        String enc_msg = "";

        for(int i = 0; i < s.length(); i++) {

            int t, j;
            int c = s.charAt(i);

            for(t = 128, j = 0; t > 0; t = t / 2, j++) {

                if((c & t) != 0) msg[((i * 8) + j)] = true;
            }
        }

        for(int i = 0; i < (msg.length / 6); i++) {

            int t, j;
            int c = 0;

            for(t = 32, j = 0; t > 0; t = t / 2, j++) {

                if(msg[((i * 6) + j)]) c += t;
            }

            if(c == 0) c = 64;
            enc_msg += base64Alphabet[c];
        }
        return new String(enc_msg);
    }
}
