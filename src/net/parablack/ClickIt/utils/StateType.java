package net.parablack.ClickIt.utils;

import net.parablack.MiniGame.api.game.IGameManager.GameState;

public interface StateType {
	public GameState getGameState();
}
