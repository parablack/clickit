package net.parablack.ClickIt.strings;


public class StringManager {
	
	public static String prefix  = "�3[�aClickIt�3] ";
	public static String prefix_spectator = "�7[�4�lX�7] �r";
	
	public static String m_Won_Pre = prefix + "�4Thanks for playing ClickIt by parablack!"; 
	public static String m_Map_Map = prefix + "�bMap: �5%s";
	public static String m_Map_Creator = prefix + "�bCreator: �5%s";
	public static String m_Map_Link = prefix + "�bLink: �5%s";
	
//	public static String p_Won_Pre = prefix +"�9�k############################################";
	
	public static String i_Redstone = "�cReturns to Hub";
	public static String i_enderpearl = "�aSpectate"; 
	public static String i_Chest = "�aInventory";
	public static String i_Emerald = "�6PREMIUM�a-Inventory";
	public static String i_clickit = "�aClickIt";
	public static String i_clickit_prem = "�aClickIt: �6EXTRA-INVENTORY"; 
	public static String i_Watch = "�1Starts the game!"; 
	
	public static String st_Feather = "�bPoison / Vergiftung (Range: 5)";
	public static String st_Flint =  "�bFire / Feuer (Range: 5)";
	public static String st_GoldNugget =  "�bExtra-Hearts / Extra-Herzen";
	public static String st_Lohenrute = "�bInvoke a Lightning";
	public static String st_MagmaCream =  "�bHealing / Heilung";
	public static String st_Bone =  "�bBlindness / Blindheit (Range: 15)";
	public static String st_SlimeBall = "�bConfusion / Verwirrung (Range: 15)";
	public static String st_String = "�bSlowness / Verlangsamung (Range: 15)";
	public static String st_Niete = "�cBLANC / NIETE";
	
	
	public static String motd_Lobby = "�5�lLOBBY";
	public static String motd_Warmup = "�8�LWARMUP";
	public static String motd_Ingame = "�8�lINGAME";
	public static String motd_Deathmatch = "�8�lDEATHMATCH";
	public static String motd_Restarting = "�4�lRESTART";
}
