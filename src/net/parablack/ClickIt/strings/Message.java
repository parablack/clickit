package net.parablack.ClickIt.strings;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public enum Message {
	

	 p_succes_Vote("�aYou succesfuly voted for this map!", true),
	 p_Welcome("�4Welcome to ClickIt", true),
	 p_Returned_to_Hub_1("�cYou were returned to the hub to make space for a premium or staff member!",  true),
	 p_Returned_to_Hub_2("�6You were returned to the hub!", true),
	 p_Started_Game("�9You started the Game!",  true),
	 p_Won("�4Congratulations! You won ClickIt and earned �a200 �4Points!",  true),
	 p_setspawn_info("�41 - 24 = Map / �425 = Deathspawn - Lobbyspawn", true),
	 p_Forcevote_Map("�aYou forced the map %s",  true),
	 p_has_now_Clicks("�7You now have �c%s �7Clicks",  true), 
	 p_stats_info("�6Detailed Stats from: �b%s",  true),
	 p_top_header("�bGlory Hall of Fame: �aClickIt",  true),
	 p_top_players("�b%s.�6: �5%s�6 - �5%s Points�6!",  true),
	 p_stats("�6%s: �a%s�6.",  true),
	 p_Now_Spectating("�aYou are now spectating %s",  true),
	 p_Earn_Points_kill("�aYou earned �620 �apoints for killing %s�a. You now have �6%s �apoints.",  true),
	 p_succes_enchanted("�aEnchanted! Your enchant costed �b%s �aClicks!",  true),
	 p_tracking("�9You are now tracking �6%s �9<�6%s �9Blocks�9>!",  true),
	 p_tl("�9Time Left: �6%s �9Seconds!",  true),
	 p_givepoints("�aYou succesfully added �6%s �apoints to %s�a. This player now has �6%s �apoints",  true),
	 p_killer_was_on("�9You killer was on �4%s Hearts�9!",  true),
	 
	 e_not_enchantable("�cThis Item is not enchantable!",  true),
	 e_already_enchanted("�cThis item is already enchanted!",  true),
	 e_enchant_permssion("�cOnly �6VIPs �cmay enchant Diamond Armor!",  true),
	 e_vote_Time("�4Voting is not active right now!",  true),
	 e_vote_Already("�4You can only vote once!",  true),
	 e_Permission("�4Acces denied!", true),
	 e_Invalid_Number("�cThis is not a valid number!", true),
	 e_Invalid_Number_Size("�cThis number is not a valid option!",  true),
	 e_Server_Error("�cThis Server has detected an Error. The Server is restarting!�r",  true),
	 e_not_Joinable("�cSorry, but this game isn't joinable now. Try it again later!�r",  true),
	 e_Server_Full("�cOnly �6VIPs �ccan join full Games!�r",  true),
	 e_random_Player_not_kicked("�aSorry, the random Player was not able to be kicked. Try again!�r",  true),
	 e_Not_Enough_Players("�cThere are not enough players to start, Countdown resetted!",  true),
	 e_World_Not_Found("�cThe world '%s' cannot be found on this server.",  true), 
	 e_Invalid_Argument_Help("�eCorrect Usage: �6%s",  false),
	 e_no_trackable("�cThere is no Player near to you!",  true),
	 
	 s_Feather("�e%s �bPlayers were poisoned!",  true), 
	 s_Flint("�e%s �bPlayers were set on fire!",  true),
	 s_GoldNugget("�e%s �bPlayers were thrown up!",  true),
	 s_Lohenrute("�bA Lightning has been invoked!",  true),
	 s_MagmaCream("�bYou are now regenerating your Life!",  true),
	 s_Bone("�e%s �bPlayers are now blind!",  true), 
	 s_SlimeBall("�e%s �bPlayers were confused!",  true),
	 s_String("�e%s �bPlayers are now slow!",  true),
	 s_unenabled("�cThe Grace period has not ended yet!",  true),

	 m_Restart("�6This server is restarting!�r", true),
	 m_Restart_Pre("�4This Server will restart in 15 seconds!�r",  true),
	 m_Maps_Vote("�6Vote for a Map with �e/vote <ID>�6: ",  true),
	 m_Player_has_died("%s �6has died!",  true), 
	 m_Players_remain("�e%s �bplayers remaining!",  true),
	 m_Player_Joined("%s�e joined the game �7(%s/16)",  true), 
	 m_Player_Left("%s�e left the game.",  true), 
	 m_Tl_Lobby("�bTeleporting to the arena in �e%s",  true),
	 m_Tl_Warmup("�e%s �bseconds until grace period ends!", true),
	 m_Tl_Ingame("�e%s �bminutes until the game ends!",  true),
	 m_Grace_Over("�4Grace period has ended! Prepare to fight!",  true), 
	 m_Map_will_play("�bThis Map will be played: �6%s",  true),
	 m_has_won("�4The round has ended! �6%s �4has won the Game!",  true),
	 m_no_winner("�4The round has ended! �4There was no Winner.",  true),
	 m_now_compass("�9All remaining players received a �aTracker�9!",  true),
	 m_nowreduced_health("�9All remaining players now have �55 Hearts�9!",  true),

	 
	 c_No_Clicks("�cYou don't have any Clicks left!",  true),
	 c_Earn_1_Click("�7You earned �c1 �7Click. You now have �c%s�7 Clicks!",  true),
	 c_Earn_2_Click("�7You earned �c2 �7Clicks. You now have �c%s�7 Clicks!",  true),
	 c_Earn_3_Click("�7You earned �c3 �7Clicks. You now have �c%s�7 Clicks!", true),
	 c_Earn_5_Click("�7You earned �c5 �7Clicks. You now have �c%s�7 Clicks!",  true),

	 a_pre("�4�k!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",  false),
	 a_earned("�aAchievement earned: �6%s",  false),
	 a_earned_description("�c� �6%s",  false),
	 a_earned_points("�4+%s �aAchievement Points!",  false);
	
	private String message;
	
	private Message(String message, boolean prefix){
		this.message = (prefix ? StringManager.prefix : "") + message;
	}
	
	public void send(CommandSender p, Object... extras){
	//	System.out.println(p.getName() + " " + message);
		p.sendMessage(String.format(message, extras));
	}
	@SuppressWarnings("deprecation")
	public void broadcast(Object... extras){
		for(Player p : Bukkit.getOnlinePlayers()){
			send(p, extras);
		}
	}
	
	public String getMessage() {
		return message;
	}
	
}
