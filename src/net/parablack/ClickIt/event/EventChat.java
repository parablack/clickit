package net.parablack.ClickIt.event;

import net.parablack.API.api.SpectatorAPI;
import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class EventChat extends GameEvent<AsyncPlayerChatEvent> {

	@EventHandler
	public void onEvent(AsyncPlayerChatEvent e) {

		PluginPlayer pl = SC.from(e.getPlayer());
		System.out.println("PP null: "+pl + " # " + Achievements.goodgame + " # " + pl.getUser());
		if (e.getMessage().equalsIgnoreCase("gg"))
			Achievements.goodgame.gainAchievement(pl.getUser().getName());
		if (pl.isSpectating() && SC.gameState() != GameState.AFTER_GAME) {
			for (IPluginPlayer p : SpectatorAPI.getSpectators()) {
				p.getPlayer().getPlayer().sendMessage(StringManager.prefix_spectator
						+ e.getFormat().substring(0, e.getFormat().length() - 4)
						+ ChatColor.GRAY + e.getMessage());

			}
			e.setCancelled(true);
		}
	}
}
