package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class EventPickupItem extends GameEvent<PlayerPickupItemEvent> {
	@EventHandler
	public void onEvent(PlayerPickupItemEvent e){
		 IPluginPlayer pl = from(e.getPlayer());
		if(pl.isSpectating()){
			e.setCancelled(true);
		}
		if(SC.gameState() == GameState.LOBBY) e.setCancelled(true);
	}
}
