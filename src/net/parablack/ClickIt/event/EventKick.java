package net.parablack.ClickIt.event;

import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.servermanager.ServerState;
import me.kombustorlp.gameapi.servermanager.StateValue;
import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerKickEvent;

public class EventKick extends GameEvent<PlayerKickEvent> {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEvent(PlayerKickEvent e) {
		PluginPlayer pl = from(e.getPlayer());

		e.setLeaveMessage("");
		int plonline = GameAPI.getPlayers().size() - 1;

		if (SC.gameState() == GameState.LOBBY) {
			Message.m_Player_Left.broadcast(e.getPlayer().getDisplayName());
			if (plonline != Bukkit.getMaxPlayers())
				ClickIt.getPluginServer().set(StateValue.SERVERSTATE,
						ServerState.JOIN.getDatabaseName());
			else
				ClickIt.getPluginServer().set(StateValue.SERVERSTATE,
						ServerState.PREMIUM.getDatabaseName());

		} else if (SC.gameState() == GameState.INGAME
				|| SC.gameState() == GameState.WARMUP) {
			if (!pl.isSpectating()) {
				if (pl.getLastHitted() != null) {
					PluginPlayer killer = from(pl
							.getLastHitted());
					killer.hasKilled(e.getPlayer());

				}
			}

			ClickIt.getInstance().getGameManager().checkWinner();
		}

		pl.destroy();

		ClickIt.getPluginServer().set(StateValue.PLAYERSONLINE,
				Bukkit.getOnlinePlayers().length - 1 + "");
	}
}
