package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerExpChangeEvent;


public class EventXP extends GameEvent<PlayerExpChangeEvent> {

	@EventHandler
	@Override
	public void onEvent(PlayerExpChangeEvent e) {
		
		 IPluginPlayer pl = from(e.getPlayer());
			if(pl.isSpectating()) e.setAmount(0);
	}
}