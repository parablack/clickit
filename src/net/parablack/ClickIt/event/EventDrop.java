package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.ClickIt;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;


public class EventDrop extends GameEvent<PlayerDropItemEvent> {

	@EventHandler
	public void onEvent(PlayerDropItemEvent e) {
		if(e.getItemDrop() == null) return;
		if(e.getItemDrop().getType() == EntityType.DROPPED_ITEM) {
			if(ClickIt.getInstance().getGameManager().getGameState() == GameState.LOBBY) e.setCancelled(true);
			if(e.getPlayer().getGameMode() == GameMode.CREATIVE) e.setCancelled(false);
		}
	}
}
