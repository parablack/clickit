package net.parablack.ClickIt.event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;


public class EventEnchanter extends GameEvent<PlayerInteractEvent> {
	private static HashMap<Material, Enchantment[]> enchantable = new HashMap<>();
	private static ArrayList<Material> premiumEnchant = new ArrayList<>();

	static {
		Enchantment[] sword_enchantments = { Enchantment.KNOCKBACK, Enchantment.DAMAGE_ALL, Enchantment.KNOCKBACK };
		Enchantment[] bow_enchantments = { Enchantment.ARROW_KNOCKBACK, Enchantment.ARROW_DAMAGE, Enchantment.ARROW_KNOCKBACK };
	//	Enchantment[] defense_enchantments = { Enchantment.PROTECTION_PROJECTILE, Enchantment.PROTECTION_ENVIRONMENTAL, Enchantment.PROTECTION_EXPLOSIONS };

		premiumEnchant.addAll(Arrays.asList(new Material[] { Material.DIAMOND_BOOTS, Material.DIAMOND_LEGGINGS, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_HELMET }));

		enchantable.put(Material.WOOD_SWORD, sword_enchantments);
		enchantable.put(Material.GOLD_SWORD, sword_enchantments);
		enchantable.put(Material.STONE_SWORD, sword_enchantments);
		enchantable.put(Material.IRON_SWORD, sword_enchantments);
		enchantable.put(Material.DIAMOND_SWORD, sword_enchantments);

//		enchantable.put(Material.LEATHER_BOOTS, defense_enchantments);
//		enchantable.put(Material.LEATHER_LEGGINGS, defense_enchantments);
//		enchantable.put(Material.LEATHER_CHESTPLATE, defense_enchantments);
//		enchantable.put(Material.LEATHER_HELMET, defense_enchantments);
//		enchantable.put(Material.GOLD_BOOTS, defense_enchantments);
//		enchantable.put(Material.GOLD_LEGGINGS, defense_enchantments);
//		enchantable.put(Material.GOLD_CHESTPLATE, defense_enchantments);
//		enchantable.put(Material.GOLD_HELMET, defense_enchantments);
//		enchantable.put(Material.CHAINMAIL_BOOTS, defense_enchantments);
//		enchantable.put(Material.CHAINMAIL_LEGGINGS, defense_enchantments);
//		enchantable.put(Material.CHAINMAIL_CHESTPLATE, defense_enchantments);
//		enchantable.put(Material.CHAINMAIL_HELMET, defense_enchantments);
//		enchantable.put(Material.IRON_BOOTS, defense_enchantments);
//		enchantable.put(Material.IRON_LEGGINGS, defense_enchantments);
//		enchantable.put(Material.IRON_CHESTPLATE, defense_enchantments);
//		enchantable.put(Material.IRON_HELMET, defense_enchantments);
//		enchantable.put(Material.DIAMOND_BOOTS, defense_enchantments);
//		enchantable.put(Material.DIAMOND_LEGGINGS, defense_enchantments);
//		enchantable.put(Material.DIAMOND_CHESTPLATE, defense_enchantments);
//		enchantable.put(Material.DIAMOND_HELMET, defense_enchantments);

		enchantable.put(Material.BOW, bow_enchantments);
	}

	@EventHandler
	public void onEvent(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			
			if(e.getItem() == null) return;
			if(e.getClickedBlock() == null) return;
			if(e.getMaterial() == null) return;
			
			if(SC.gameState() != GameState.WARMUP && SC.gameState() != GameState.INGAME) return;
			if(e.getClickedBlock().getType() != Material.ENCHANTMENT_TABLE) return;
			PluginPlayer pl = from(e.getPlayer());
			e.setCancelled(true);
			if(pl.isSpectating()) return;
			

			
			
			if(pl.getClicks() < 3) {
				Message.c_No_Clicks.send(e.getPlayer());
				return;
			}
			if(!e.getItem().getEnchantments().isEmpty()) {
				Message.e_already_enchanted.send(e.getPlayer());
				return;
			}
			if(!enchantable.containsKey(e.getItem().getType())) {
				Message.e_not_enchantable.send(e.getPlayer());
				return;
			}
//			if(premiumEnchant.contains(e.getItem().getType()) && !e.getPlayer().hasPermission("clickit.premiumenchant")) {
//				Message.e_enchant_permssion.send(e.getPlayer());
//				return;
//			}

			int clicks = pl.getClicks();

			int enchantment = (int) (Math.random() * 3);
			int clicksRemove = 3 + ((int) (Math.random() * 3));
			if(clicksRemove > clicks) clicksRemove = clicks;

			Enchantment[] possibleEnchants = enchantable.get(e.getItem().getType());
			Enchantment toAdd = possibleEnchants[enchantment];
			if(toAdd == Enchantment.ARROW_DAMAGE) Achievements.powerbow.gainAchievement(pl.getUser().getName());
			e.getItem().addEnchantment(toAdd, 1);
			int newClicks = clicks - clicksRemove;
			pl.setClicks(newClicks);
			Message.p_succes_enchanted.send(e.getPlayer(), clicksRemove);
			e.getPlayer().setLevel(clicks);
		}
	}
}