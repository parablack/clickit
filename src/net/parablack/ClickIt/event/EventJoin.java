package net.parablack.ClickIt.event;

import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.servermanager.ServerState;
import me.kombustorlp.gameapi.servermanager.StateValue;
import net.parablack.API.api.SpectatorAPI;
import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;
import net.parablack.MiniGame.api.map.IMap;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.WeatherType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;


public class EventJoin extends GameEvent<PlayerJoinEvent> {
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEvent(final PlayerJoinEvent e) {
		PluginPlayer pl = from(e.getPlayer());
	//	System.out.println("USN: " + pl.getUser().getDisplayName());
		e.getPlayer().setDisplayName(pl.getUser().getColoredName());
	//	System.out.println("USN: " + e.getPlayer().getDisplayName());
		
		
		int plonline = GameAPI.getPlayers().size();

		Player p = e.getPlayer();
		
		p.getInventory().clear();
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		p.setFallDistance(0);
		p.setLevel(0);
		p.setExp(0);
		p.setFireTicks(0);
		p.setFoodLevel(20);
		p.setSaturation(1F);
		p.setMaxHealth(20.0);
		p.setHealth(20.0);
		p.setAllowFlight(false);
		p.setFlying(false);
		p.setPlayerWeather(WeatherType.CLEAR);

		for (PotionEffect p1 : p.getActivePotionEffects())
			p.removePotionEffect(p1.getType());
		
		ClickIt.getPluginServer().set(StateValue.PLAYERSONLINE, Bukkit.getOnlinePlayers().length + "");
		e.setJoinMessage(null);
		if (SC.gameState() == GameState.INGAME) {
			pl.setSpectator();
			for(Player p1 : Bukkit.getOnlinePlayers()){
				System.out.println( e.getPlayer().getName() +" <-- " + p1.getPlayer().getName()  );
				p1.hidePlayer(pl.getPlayer());
			}
			for(PluginPlayer p1 : SpectatorAPI.getSpectators()){
				System.out.println( e.getPlayer().getName() +" --> " + p1.getPlayer().getName()  );
				e.getPlayer().hidePlayer(p1.getPlayer());
			}
			return;
		}
		
		if(SC.gameState().equals(GameState.LOBBY)){
			if (plonline != Bukkit.getMaxPlayers())
				ClickIt.getPluginServer().set(StateValue.SERVERSTATE,
						ServerState.JOIN.getDatabaseName());
			else
				ClickIt.getPluginServer().set(StateValue.SERVERSTATE,
						ServerState.PREMIUM.getDatabaseName());
		}
		if(SC.gameState() == GameState.LOBBY && ClickIt.getInstance().getGameManager().getActualCountdown().getTimeLeft() > 10){
			p.getInventory().setItem(0, me.kombustorlp.gameapi.Data.achievements);
			p.getInventory().setItem(8, me.kombustorlp.gameapi.Data.backtohub);
		}
		Message.m_Player_Joined.broadcast(e.getPlayer().getDisplayName(), Bukkit.getOnlinePlayers().length);
		Message.p_Welcome.send(e.getPlayer());
		Message.m_Maps_Vote.send(e.getPlayer());
		for(int i = 0; i < ClickIt.getInstance().getMapManager().getVotableMaps().size(); i++){
			IMap map = ClickIt.getInstance().getMapManager().getVotableMaps().get(i);
			e.getPlayer().sendMessage(StringManager.prefix + "�6" + i + ": �9" + map.getName() + "�6 (�b" + map.getVotes() + "�6).");
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(ClickIt.getInstance(), new Runnable() {

			@Override
			public void run() {
				e.getPlayer().setGameMode(GameMode.ADVENTURE);
				e.getPlayer().teleport(ClickIt.getInstance().getMapManager().getLobby().getSpawn());
			}
		}, 12);
		
	}


}
