package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

public class EventBlockPlace extends GameEvent<BlockPlaceEvent> {
	@EventHandler
	@Override
	public void onEvent(BlockPlaceEvent e) {
	
		if(e.getBlock().getType() == Material.CAKE_BLOCK) return;
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE) e.setCancelled(true);
	}

}
