package net.parablack.ClickIt.event;

import net.parablack.API.api.SpectatorAPI;
import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.exception.PlayerNotRegisteredException;
import net.parablack.ClickIt.strings.Message;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;


public class EventDeath extends GameEvent<PlayerDeathEvent> {
	@EventHandler
	@SuppressWarnings("deprecation")
	public void onEvent(PlayerDeathEvent e) {
		
		e.setKeepLevel(true);
		
		if (ClickIt.getInstance().getGameManager().getGameState() == GameState.INGAME) {
			
			
			Player p = e.getEntity();
			PluginPlayer pl = from(p);
			p.setLevel(0);
			pl.die();
			for (Player p1 : Bukkit.getOnlinePlayers()) {
				p1.hidePlayer(p);
			}
			
			e.setDeathMessage(null);
		//	if((boolean) GameSetting.getSetting("showdeath").getSetting())
			Message.m_Player_has_died.broadcast(p.getDisplayName());
			Message.m_Players_remain.broadcast(SpectatorAPI.getAlive().size() + "");
			
			if(e.getEntity().getLastDamageCause().getCause() == DamageCause.FIRE_TICK || e.getEntity().getLastDamageCause().getCause() == DamageCause.FIRE) Achievements.diefire.gainAchievement(pl.getUser().getName());;
			
			pl.addDeath();
			
			if(pl.getDeaths() == 100) Achievements.diehundred.gainAchievement(pl.getUser().getName());
			if (p.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
				if (p.getKiller() != null) {
					try{
					PluginPlayer killer = from(p.getKiller());
					killer.hasKilled(p);
					}
					catch(PlayerNotRegisteredException ex){
						System.out.println("Unnormal value in PlayerDeathEvent, skipping");
					}
	//				Message.p_killer_was_on.send(p, ((CraftPlayer) p.getKiller()).getHandle().getHealth());
				}
			} else {
				if (pl.getLastHitted() != null) {
					PluginPlayer killer = from(pl.getLastHitted());

					killer.hasKilled(p);
	//				PlayerManager.earnPointsforKill(PlayerManager.lastHitted.get(p), p);
				}
			}
			ClickIt.getInstance().getGameManager().checkItems();
			ClickIt.getInstance().getGameManager().checkWinner();
		}

	}

}
