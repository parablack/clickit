package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class EventCommandPre extends GameEvent<PlayerCommandPreprocessEvent>{
	@EventHandler
	public void onEvent(PlayerCommandPreprocessEvent e){
		if(e.getPlayer().hasPermission("clickit.command.stop") && e.getMessage().contains("/stop")){
			e.getPlayer().sendMessage("�4Usage of �6/stop �4will stop this server without setting his state! Use �a/permstop �4instead!");
			e.setCancelled(true);
		}
	}
}
