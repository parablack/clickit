package net.parablack.ClickIt.event;

import java.util.HashMap;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.server.PlayerTrack;
import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;


public class EventCompass extends GameEvent<PlayerInteractEvent> {

	public static HashMap<Player, Player> trackedPlayers = new HashMap<>();

	@EventHandler
	public void onEvent(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getMaterial() == Material.COMPASS) {
				Object[] playerDetails = getNearestPlayer(e.getPlayer());
				if(playerDetails == null) {
					Message.e_no_trackable.send(e.getPlayer());
					return;
				}
				Player nearestPlayer = (Player) playerDetails[0];
				int distance = (int) playerDetails[1];
				e.getPlayer().setCompassTarget(nearestPlayer.getLocation());
				new PlayerTrack(e.getPlayer(), nearestPlayer);
				Message.p_tracking.send(e.getPlayer(), nearestPlayer.getDisplayName(), distance);
			}
		}
	}

	protected Object[] getNearestPlayer(Player p) {
		for (int i = 0; i < 200; i++) {
			for (Entity e1 : p.getNearbyEntities(i, 60, i)) {

				if(e1.getType() == EntityType.PLAYER) {
					Player t = (Player) e1;
					if(p.canSee(t)) return new Object[] { t, i };
				}
			}
		}
		return null;
	}

}
