package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;


public class EventInventory extends GameEvent<InventoryClickEvent> {
	boolean resItems = false;
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEvent(final InventoryClickEvent e){
		if(!(e.getWhoClicked() instanceof Player)) return;
		PluginPlayer pl = from((Player) e.getWhoClicked());
		if((SC.gameState() == GameState.LOBBY || pl.isSpectating()) && e.getWhoClicked().getGameMode() != GameMode.CREATIVE) e.setCancelled(true);
		
		if(e.getInventory().getName().contains("Click")){
			if(e.getCurrentItem() == null) return;	
			if(e.getCurrentItem().getType()==Material.STAINED_GLASS_PANE && e.getCurrentItem().getData().getData() == DyeColor.RED.getData()){ e.setCancelled(true); resItems = true; }
			if(e.getCurrentItem().getType()==Material.STAINED_GLASS_PANE && e.getCurrentItem().getData().getData() == DyeColor.BLACK.getData()){
				Player p = (Player) e.getWhoClicked();
				e.setCancelled(true);
			
				if(pl.getClicks()>0){
					ItemStack clicked = ClickIt.getInstance().getGameManager().getMasterInv().getItem(e.getSlot());
					e.getInventory().setItem(e.getSlot(), clicked);
					pl.addClicks(-1);
					if(pl.getClicks() == 0 && clicked.getType() == Material.IRON_SWORD) Achievements.luckyoff.gainAchievement(pl.getUser().getName());
					Message.p_has_now_Clicks.send(p, pl.getClicks() +"");
				}else{
					Message.c_No_Clicks.send(p);
				}
				resItems = true;
				p.setLevel(pl.getClicks());
			}
			
		}
	}
	
}
