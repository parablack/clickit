package net.parablack.ClickIt.event;

import me.kombustorlp.gameapi.misc.Rang;
import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;


public class EventInventorySpectate extends GameEvent<InventoryClickEvent> {

	@EventHandler
	public void onEvent(InventoryClickEvent e) {

		Player p = (Player) e.getWhoClicked();
		PluginPlayer pl = from(p);
		if(pl.isSpectating()) {
			if(e.getCurrentItem() == null) return;
			if(e.getCurrentItem().getType() == Material.RED_ROSE) {
				e.setCancelled(true);
				Player totele = ClickIt.getInstance().getSkulls().get(e.getCurrentItem());

				p.teleport(totele);
				if(from(totele).getUser().getRang().equals(Rang.YOUTUBER)) Achievements.spectateyt.gainAchievement(pl.getUser().getName());;
				Message.p_Now_Spectating.send(p, totele.getDisplayName());
				e.getWhoClicked().closeInventory();
			}
		}

	}

}
