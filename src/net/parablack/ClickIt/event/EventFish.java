package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;

public class EventFish extends GameEvent<PlayerFishEvent>{

	@EventHandler
	public void onEvent(PlayerFishEvent e){
		PluginPlayer pl = from(e.getPlayer());
		if(e.getState() == State.CAUGHT_FISH) Achievements.fisher.gainAchievement(pl.getUser().getName());
	}
}
