package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.exception.PlayerNotRegisteredException;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;


public class EventChest extends GameEvent<PlayerInteractEvent> {

	@EventHandler
	@SuppressWarnings("deprecation")
	public void onEvent(PlayerInteractEvent e) {

		if(e.getAction() == Action.LEFT_CLICK_BLOCK && e.getPlayer().getGameMode() != GameMode.CREATIVE) {
			e.setCancelled(true);
		}
		
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block b = e.getClickedBlock();
			if(b.getType() == Material.WORKBENCH || b.getType() == Material.ANVIL || b.getType() == Material.BED) {
				e.setCancelled(true);
			}
		}
		
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			PluginPlayer pl;
			try{
				pl = from(e.getPlayer());
			}catch(PlayerNotRegisteredException exc){
				return;
				//Thrown when player is using redstone, can be ignored
			}
			
			if(pl.isSpectating()) {
				e.setCancelled(true);
				return;
			}

			if(SC.gameState() == GameState.WARMUP || SC.gameState() == GameState.INGAME) {
				
				Player p = e.getPlayer();
				
				if(e.getClickedBlock().getType() == Material.WOOL && e.getClickedBlock().getData() == 5) {
					pl.addClicks(1);
					Message.c_Earn_1_Click.send(e.getPlayer(), pl.getClicks() + "");
					p.setLevel(pl.getClicks());
					e.getClickedBlock().setType(Material.AIR);
				}
				if(e.getClickedBlock().getType() == Material.WOOL && e.getClickedBlock().getData() == 4) {
					pl.addClicks(2);
					Message.c_Earn_2_Click.send(e.getPlayer(), pl.getClicks() + "");
					p.setLevel(pl.getClicks());
					e.getClickedBlock().setType(Material.AIR);
				}
				if(e.getClickedBlock().getType() == Material.WOOL && e.getClickedBlock().getData() == 11) {
					pl.addClicks(3);
					Message.c_Earn_3_Click.send(e.getPlayer(), pl.getClicks() + "");
					p.setLevel(pl.getClicks());
					e.getClickedBlock().setType(Material.AIR);
				}
				if(e.getClickedBlock().getType() == Material.WOOL && e.getClickedBlock().getData() == 14) {
					pl.addClicks(5);
					Message.c_Earn_5_Click.send(e.getPlayer(), pl.getClicks() + "");
					p.setLevel(pl.getClicks());
					e.getClickedBlock().setType(Material.AIR);
				}
				if(pl.getClicks() >= 100){
					if(!ClickIt.getInstance().getMapManager().getPlayedMap().getMeta().toLowerCase().contains("noclickachievement"))
					Achievements.hundredclicks.gainAchievement(pl.getUser().getName());
				}
			}

		}
	}
}
