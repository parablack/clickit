package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;


public class EventDamage extends GameEvent<EntityDamageEvent> {
	@EventHandler
	@Override
	public void onEvent(EntityDamageEvent e) {
		if(e.getEntity() == null || e.getEntityType() == null) return;

		if(e.getEntityType() == EntityType.PLAYER) {
			if(SC.gameState() != GameState.INGAME) {
				e.setCancelled(true);
			}
			Player p = (Player) e.getEntity();

			if(from(p).isSpectating()) {
				e.setCancelled(true);
			}

		}

		if(ClickIt.getInstance().getMapManager().getPlayedMap() == null) return;

		if(ClickIt.getInstance().getMapManager().getPlayedMap().getMeta().toLowerCase().contains("nodrowning") && e.getCause() == DamageCause.DROWNING) {
			e.setCancelled(true);
		}

	}
}
