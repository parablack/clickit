package net.parablack.ClickIt.event;


import net.parablack.API.event.GameEvent;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerRespawnEvent;


public class EventRespawn extends GameEvent<PlayerRespawnEvent> {

	@EventHandler
	public void onEvent(PlayerRespawnEvent e){
		from(e.getPlayer()).setSpectator();
	}
}
