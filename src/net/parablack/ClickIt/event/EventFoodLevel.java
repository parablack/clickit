package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.FoodLevelChangeEvent;


public class EventFoodLevel extends GameEvent<FoodLevelChangeEvent> {


	@EventHandler
	public void onEvent(FoodLevelChangeEvent e) {
		if(!(e.getEntity() instanceof Player)) return;
		if(SC.gameState() != GameState.INGAME) {
			e.setCancelled(true);
		}
		IPluginPlayer pl = from((Player) e.getEntity());
		if(pl.isSpectating()) e.setCancelled(true);

	}
}
