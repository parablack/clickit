package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.GameMode;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;


public class EventDamageEntity extends GameEvent<EntityDamageByEntityEvent> {

	@EventHandler
	public void onEvent(EntityDamageByEntityEvent e) {
		if(SC.gameState() == GameState.INGAME) {
			if(!(e.getEntity() instanceof Player)) {
				return;
			}
			if(!(e.getDamager() instanceof Player)) {
				if(e.getDamager() instanceof Arrow) e.setDamage(e.getDamage() * 2);
				return;
			}

		}
		if(!(e.getEntity() instanceof Player) || !(e.getDamager() instanceof Player)) return;
		PluginPlayer damager = SC.from((Player) e.getDamager());
		PluginPlayer hitted = SC.from((Player) e.getEntity());

		if(!e.isCancelled()) hitted.setLastHitted((Player) e.getDamager());;

		if(damager.isSpectating()) {
			Player p = (Player) e.getDamager();
			if(p.getGameMode() == GameMode.CREATIVE) {
				return;
			}
			e.setCancelled(true);
		}
	}
	

}
