package net.parablack.ClickIt.event;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerLevelChangeEvent;


public class EventLevelChange extends GameEvent<PlayerLevelChangeEvent> {

	@EventHandler
	public void onEvent(PlayerLevelChangeEvent e) {
		if(SC.gameState() == GameState.WARMUP || SC.gameState() == GameState.INGAME) {
			from(e.getPlayer()).setClicks(e.getNewLevel());
		}
	}
}
