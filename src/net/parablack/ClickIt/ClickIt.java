package net.parablack.ClickIt;

import java.util.HashMap;

import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.manager.StatManager;
import me.kombustorlp.gameapi.servermanager.Server;
import net.parablack.API.api.IStringManager;
import net.parablack.API.api.SpectatorAPI;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.command.Command_begingame;
import net.parablack.ClickIt.command.Command_clickit;
import net.parablack.ClickIt.command.Command_csstop;
import net.parablack.ClickIt.command.Command_forcevote;
import net.parablack.ClickIt.command.Command_givepoints;
import net.parablack.ClickIt.command.Command_inv;
import net.parablack.ClickIt.command.Command_stats;
import net.parablack.ClickIt.command.Command_stop;
import net.parablack.ClickIt.command.Command_tl;
import net.parablack.ClickIt.command.Command_vote;
import net.parablack.ClickIt.event.EventArrow;
import net.parablack.ClickIt.event.EventBlockBreak;
import net.parablack.ClickIt.event.EventBlockPlace;
import net.parablack.ClickIt.event.EventChat;
import net.parablack.ClickIt.event.EventChest;
import net.parablack.ClickIt.event.EventCommandPre;
import net.parablack.ClickIt.event.EventCompass;
import net.parablack.ClickIt.event.EventDamage;
import net.parablack.ClickIt.event.EventDamageEntity;
import net.parablack.ClickIt.event.EventDeath;
import net.parablack.ClickIt.event.EventDiamond;
import net.parablack.ClickIt.event.EventDrop;
import net.parablack.ClickIt.event.EventEnchanter;
import net.parablack.ClickIt.event.EventFish;
import net.parablack.ClickIt.event.EventFoodLevel;
import net.parablack.ClickIt.event.EventInventory;
import net.parablack.ClickIt.event.EventInventorySpectate;
import net.parablack.ClickIt.event.EventJoin;
import net.parablack.ClickIt.event.EventKick;
import net.parablack.ClickIt.event.EventLevelChange;
import net.parablack.ClickIt.event.EventLogin;
import net.parablack.ClickIt.event.EventPickupItem;
import net.parablack.ClickIt.event.EventQuit;
import net.parablack.ClickIt.event.EventRespawn;
import net.parablack.ClickIt.event.EventTeleportFix;
import net.parablack.ClickIt.event.EventXP;
import net.parablack.ClickIt.game.GameManager;
import net.parablack.ClickIt.game.InventoryManager;
import net.parablack.ClickIt.misc.ItemStackManager;
import net.parablack.ClickIt.server.ServerManager;
import net.parablack.ClickIt.special.SpecialBone;
import net.parablack.ClickIt.special.SpecialFeather;
import net.parablack.ClickIt.special.SpecialFlint;
import net.parablack.ClickIt.special.SpecialGoldNugget;
import net.parablack.ClickIt.special.SpecialLohenrute;
import net.parablack.ClickIt.special.SpecialMagmaCream;
import net.parablack.ClickIt.special.SpecialSlimeBall;
import net.parablack.ClickIt.special.SpecialString;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.MiniGame.api.MiniGame;
import net.parablack.MiniGame.api.inventory.ClickEffect;
import net.parablack.MiniGame.api.inventory.DefaultFunctionalInventory;
import net.parablack.MiniGame.api.inventory.IFunctionInventory;
import net.parablack.MiniGame.api.inventory.PluginInvItem;
import net.parablack.MiniGame.api.map.DefaultMapManager;
import net.parablack.MiniGame.api.map.IMapInformation;
import net.parablack.MiniGame.api.map.IMapManager;
import net.parablack.MiniGame.api.map.MapReset;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;

public class ClickIt extends MiniGame {

	public static ClickIt instance;

	private HashMap<ItemStack, Player> skulls = new HashMap<>();

	private GameManager gameManager;
	private IMapManager mapmg;

	private static Server pluginServer;

	private IFunctionInventory specInv = new DefaultFunctionalInventory();

	@Override
	public void pluginEnable() {

		pluginServer = new Server(Bukkit.getIp(), Bukkit.getPort());
		// GameAPI.setUserClass(PluginPlayer.class);
		GameAPI.initialize(new StatManager("stats_ci", new String[] { "points",
				"wins", "kills", "deaths", "gamesplayed" }), false, true,
				false, false, true, true, true, true, true, true, false);
		instance = this;
		// TODO fix
		mapmg = new DefaultMapManager(new IMapInformation() {

			@Override
			public int getVotableMaps() {
				return 3;
			}

			@Override
			public String getTableName() {
				return "clickit_maps";
			}
		});

		specInv.addItem(new PluginInvItem(ItemStackManager.getCompass(), 0,
				new ClickEffect() {

					@Override
					public void click(IPluginPlayer p, PlayerInteractEvent e) {
						Inventory inv = Bukkit.createInventory(null, 27,
								StringManager.i_enderpearl);

						for (IPluginPlayer p1 : SpectatorAPI.getAlive()) {
							ItemStack add = new ItemStack(Material.RED_ROSE, 1);
							ItemMeta im = add.getItemMeta();
							im.setDisplayName(p1.getPlayer().getDisplayName());
							add.setItemMeta(im);
							skulls.put(add, p1.getPlayer());
							inv.addItem(add);
						}
						p.getPlayer().openInventory(inv);
						e.setCancelled(true);
					}
				}));

		gameManager = new GameManager();

		ServerManager.enable();
		InventoryManager.createMasterInv();

		new Command_begingame();
		new Command_clickit();
		new Command_csstop();
		new Command_forcevote();
		new Command_givepoints();
		new Command_inv();
		new Command_stats();
		new Command_stop();
		new Command_tl();
		new Command_vote();

		// this.getCommand("csstop").setExecutor(new Command_csstop());
		// this.getCommand("begingame").setExecutor(new Command_begingame());
		// this.getCommand("setspawn").setExecutor(new Command_setspawn(this));
		// this.getCommand("vote").setExecutor(new Command_vote());
		// // this.getCommand("addmap").setExecutor(new Command_addmap(this));
		// this.getCommand("ClickIt").setExecutor(new Command_clickit());
		// this.getCommand("forcevote").setExecutor(new Command_forcevote());
		// this.getCommand("inv").setExecutor(new Command_inv());
		// this.getCommand("stats").setExecutor(new Command_stats());
		// this.getCommand("givepoints").setExecutor(new Command_givepoints());
		// this.getCommand("permstop").setExecutor(new Command_stop());
		// this.getCommand("top").setExecutor(new Command_top());
		// this.getCommand("tl").setExecutor(new Command_tl());

		PluginManager pm = Bukkit.getPluginManager();

		pm.registerEvents(new EventArrow(), this);
		pm.registerEvents(new EventBlockBreak(), this);
		pm.registerEvents(new EventBlockPlace(), this);
		pm.registerEvents(new EventBlockBreak(), this);
		pm.registerEvents(new EventChat(), this);
		pm.registerEvents(new EventChest(), this);
		pm.registerEvents(new EventCommandPre(), this);
		// pm.registerEvents(new EventSpectate(), this);
		pm.registerEvents(new EventDamage(), this);
		pm.registerEvents(new EventDamageEntity(), this);
		pm.registerEvents(new EventDeath(), this);
		pm.registerEvents(new EventDiamond(), this);
		pm.registerEvents(new EventDrop(), this);
		pm.registerEvents(new EventEnchanter(), this);
		pm.registerEvents(new EventFoodLevel(), this);
		pm.registerEvents(new EventInventory(), this);
		pm.registerEvents(new EventJoin(), this);
		pm.registerEvents(new EventKick(), this);
		pm.registerEvents(new EventLevelChange(), this);
		pm.registerEvents(new EventLogin(), this);
		pm.registerEvents(new EventPickupItem(), this);
		pm.registerEvents(new EventQuit(), this);
		// pm.registerEvents(new EventClickReturnToHubItem(), this);
		pm.registerEvents(new EventRespawn(), this);
		pm.registerEvents(new EventTeleportFix(), this);
		pm.registerEvents(new EventXP(), this);
		pm.registerEvents(new EventPickupItem(), this);
		// pm.registerEvents(new EventSpectate(), this);
		pm.registerEvents(new EventCompass(), this);
		// pm.registerEvents(new EventAchievement(), this);
		pm.registerEvents(new EventFish(), this);
		pm.registerEvents(new EventInventorySpectate(), this);

		Bukkit.getPluginManager().registerEvents(new SpecialLohenrute(), this);
		Bukkit.getPluginManager().registerEvents(new SpecialGoldNugget(), this);
		Bukkit.getPluginManager().registerEvents(new SpecialMagmaCream(), this);
		Bukkit.getPluginManager().registerEvents(new SpecialSlimeBall(), this);
		Bukkit.getPluginManager().registerEvents(new SpecialFeather(), this);
		Bukkit.getPluginManager().registerEvents(new SpecialFlint(), this);
		Bukkit.getPluginManager().registerEvents(new SpecialBone(), this);
		Bukkit.getPluginManager().registerEvents(new SpecialString(), this);

		getMapManager().getLobby();
		Achievements.init();
		GameManager.startGame();

		Bukkit.getServer().createWorld(
				new WorldCreator(getMapManager().getLobby().getWorldName()));
		Bukkit.getWorld(getMapManager().getLobby().getWorldName())
				.setDifficulty(Difficulty.PEACEFUL);
	}

	@Override
	public void pluginDisable() {
		this.saveConfig();

		// RESET MAP:
		MapReset.resetMap();
	}

	@Override
	public void pluginLoad() {
	}

	public static ClickIt getInstance() {
		return instance;
	}

	public GameManager getGameManager() {
		return gameManager;
	}

	@Override
	public IMapManager getMapManager() {
		return mapmg;
	}

	@Override
	public IFunctionInventory getSpectatorInventory() {
		return specInv;
	}

	@Override
	public MiniGame getGame() {
		return instance;
	}

	@Override
	public IStringManager getStringManager() {
		return new IStringManager() {

			@Override
			public String getPrefix() {
				return StringManager.prefix;
			}
		};
	}

	public HashMap<ItemStack, Player> getSkulls() {
		return skulls;
	}

	public static Server getPluginServer() {
		return pluginServer;
	}

	// @Override
	// public IFunctionInventory getLobbyInventory() {
	// // TODO Auto-generated method stub
	// return ;
	// }

}