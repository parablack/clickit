package net.parablack.ClickIt.command;

import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.achievement.Achievements;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.command.Command;
import org.bukkit.command.ConsoleCommandSender;


public class Command_inv extends PluginCommand {

	public Command_inv() {
		super("inv", -1);
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {

		if(cs.getPlayer() instanceof ConsoleCommandSender) return;

		if(args.length == 1) {
			if(cs.getRang().getLevel() >= Rang.DEVELOPER.getLevel()) {

				if(args[0].equalsIgnoreCase("normal")) {
					cs.getPlayer().openInventory(ClickIt.getInstance().getGameManager().masterInv);
				} else Message.e_Invalid_Argument_Help.send(cs.getPlayer(), "/inv [normal]");
			}
		} else {
			if(ClickIt.getInstance().getGameManager().getGameState() == GameState.LOBBY) Achievements.lobbyclicker.gainAchievement(cs.getName());;
			cs.getPlayer().openInventory(SC.from(cs.getPlayer()).getClickInventory());
		}

	}
	
}
