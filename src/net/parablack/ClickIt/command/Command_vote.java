package net.parablack.ClickIt.command;

import me.kombustorlp.gameapi.cmds.utils.Descriptable;
import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.MiniGame.api.map.IMap;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;


@Descriptable(usage = "<ID>")
public class Command_vote extends PluginCommand {

	public Command_vote() {
		super("vote", -1);
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {

	//	if(cs.getSender() instanceof ConsoleCommandSender) return;

		Player p = cs.getPlayer();

		if(args.length == 0) {

			Message.m_Maps_Vote.send(p);
			int j = 0;
			for (IMap i : ClickIt.getInstance().getMapManager().getVotableMaps()) {
				p.sendMessage(StringManager.prefix + "�6" + j + ": �9" + i.getName() + "�6 (�b" + i.getVotes() + "�6).");
				j++;
			}

		} else if(args.length == 1) {
			int voted = Integer.parseInt(args[0]);

			if(voted < 0 || voted >= ClickIt.getInstance().getMapManager().getVotableMaps().size()) {
				Message.e_Invalid_Number_Size.send(p);
				return;
			}
			if(!ClickIt.getInstance().getMapManager().isVoteEnabled()) {
				Message.e_vote_Time.send(p);
				return;
			}
			if(ClickIt.getInstance().getMapManager().hasVoted(p.getUniqueId().toString())) {
				Message.e_vote_Already.send(p);
				return;
			}
			ClickIt.getInstance().getMapManager().setVoted(p.getUniqueId().toString());

			ClickIt.getInstance().getMapManager().getVotableMaps().get(voted).vote();;
			Message.p_succes_Vote.send(p);

		} else {
			usageFucker();
		}
	}

}
