package net.parablack.ClickIt.command;



import me.kombustorlp.gameapi.cmds.utils.Descriptable;
import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.strings.Message;
import net.parablack.ClickIt.utils.SC;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

@Descriptable(usage = "<Player> <Points>")
public class Command_givepoints extends PluginCommand {


	public Command_givepoints() {
		super("givepoints", Rang.ADMIN.getLevel());
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
		if(args.length!=2) usageFucker();
		
		Player toGive = Bukkit.getPlayer(args[0]);
		
		int pointsadd = Integer.parseInt(args[1]);
		PluginPlayer pl = SC.from(toGive);
		pl.addPoints(pointsadd);
		
		Message.p_givepoints.send(cs.getPlayer(), pointsadd, toGive.getName(), pl.getPoints());
	}
	
}
