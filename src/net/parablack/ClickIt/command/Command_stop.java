package net.parablack.ClickIt.command;

import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.servermanager.ServerState;
import me.kombustorlp.gameapi.servermanager.StateValue;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.StringManager;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;


public class Command_stop extends PluginCommand {
	public Command_stop() {
		super("permstop", Rang.ADMIN.getLevel());
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
		if(args.length == 0 || (args.length == 1 && !args[0].equals("really"))) {
			cs.sendMessage(StringManager.prefix + "�6/permstop �4will permanently CLOSE this server! If you want this, use �a/permstop really");
		}
		if(args.length == 1 && args[0].equals("really")) {
			ClickIt.getPluginServer().set(StateValue.SERVERSTATE, ServerState.OFFLINE.getDatabaseName());
	//		ClickIt.getInstance().getGameConnector().getServer().setState(ServerState.OFFLINE);
			Bukkit.getServer().shutdown();
		}

	}
}