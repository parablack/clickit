package net.parablack.ClickIt.command;

import java.util.Timer;
import java.util.TimerTask;

import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.server.ServerManager;
import net.parablack.ClickIt.strings.Message;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class Command_csstop extends PluginCommand {
	

	public Command_csstop() {
		super("csstop", Rang.ADMIN.getLevel());
	}


	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
		Timer t = new Timer();
		t.schedule(new TimerTask() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers()){
					ServerManager.kickPlayer(p, Message.m_Restart.getMessage());
				}
				ClickIt.getInstance().getGameManager().setGameState(GameState.RESTART);
				ServerManager.restartServerRawDelayed(200);
				
			}
		}, 300);
	}
}
