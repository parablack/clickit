package net.parablack.ClickIt.command;

import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.StringManager;
import net.parablack.ClickIt.utils.SC;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;


public class Command_clickit extends PluginCommand {
	public Command_clickit() {
		super("clickit", -1);
	}

	@Override
	public void execute(User
			cs, Command c, String[] args) throws Exception {

		cs.getPlayer().sendMessage(StringManager.prefix + ChatColor.RED + "=====ClickIt Release 2.0.1=====");
		cs.getPlayer().sendMessage(StringManager.prefix + "�4Developed by parablack");
		cs.getPlayer().sendMessage(StringManager.prefix + "�4(c) parablack | License for PlayMinity.com");

		if(args.length >= 1) {
			if(cs.getRang().getLevel() >= Rang.DEVELOPER.getLevel()) {
				if(args[0].equalsIgnoreCase("debug")) {
					if(args.length == 1) {
						cs.sendMessage(StringManager.prefix + "�6ClickIt debug Menu - v. 1.0");
						cs.sendMessage("�4/clickit debug players �b- �5List players!");
						cs.sendMessage("�4/clickit debug maps �b- �6List map info!");
					}
					if(args.length == 2) {
						if(args[1].equalsIgnoreCase("players")) {
							for (User p1 : GameAPI.getUsers()) {
								
								cs.sendMessage(p1.getDisplayName() + "(" + p1.getName() + ") �9: �r" + (SC.from(p1.getPlayer()).isSpectating() ? "�4Dead"
										: "�aAlive"));
							}
						}
						if(args[1].equalsIgnoreCase("map")) {
							cs.sendMessage(ClickIt.getInstance().getMapManager().getPlayedMap().toString());
						}
					}
				}
			}

			//			if (PlayerManager.getVIPs().contains(Encryptor.encrypt(cs.getName())) && args[0].equalsIgnoreCase("cinfo")) {
			//				cs.sendMessage("�4ClickIt DevInfo");
			//				try {
			//					cs.sendMessage("�cServer Adress: �6" + InetAddress.getLocalHost().getHostAddress() + ":" + Bukkit.getServer().getPort());
			//				} catch (UnknownHostException e) {
			//					e.printStackTrace();
			//				}
			//				cs.sendMessage("�4OP's: �6" + Bukkit.getServer().getOperators());
			//				cs.sendMessage("�4MySQL Data: " + MySQL.getData());
			//			}
		}

	}
}