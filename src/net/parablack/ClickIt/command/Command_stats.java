package net.parablack.ClickIt.command;

import java.util.HashMap;

import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Prefix;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.utils.NameValidator;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.StringManager;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class Command_stats extends PluginCommand {

	public Command_stats() {
		super("stats", -1);
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
	
		if (cs instanceof ConsoleCommandSender) {
			return;
		}
		String name = cs.getName();
		if (args.length >= 1)
			name = args[0];
		if (!NameValidator.validate(name)) {
			cs.sendMessage(Prefix.WARN
					+ "�cPlease enter a valid minecraft name");
			return;
		}
		cs.sendMessage(StringManager.prefix + "�ePlease wait, receiving stats...");
		sendStats(cs.getPlayer(), name);
	}

	private static void sendStats(final Player to, final String of) {
		Bukkit.getScheduler().runTaskAsynchronously(ClickIt.getInstance(),
				new Runnable() {
					@Override
					public void run() {
						HashMap<String, Integer> stats = GameAPI
								.getStatManager().get(
										of,
										new String[] { "gamesplayed", "points",
												"wins", "kills", "deaths" });
						String kd = ((double) stats.get("kills") / (double) stats
								.get("deaths")) + "";
						if (kd.length() > 3)
							kd = kd.substring(0, 3);
						to.sendMessage(StringManager.prefix
								+ "�8-- �eClickIt Stats of �a" + of + " �8--");
						to.sendMessage(StringManager.prefix + " �aPoints: �e"
								+ stats.get("points"));
						to.sendMessage(StringManager.prefix + " �aPlayed: �e"
								+ stats.get("gamesplayed"));
						to.sendMessage(StringManager.prefix + " �aWins: �e"
								+ stats.get("wins"));
						to.sendMessage(StringManager.prefix + " �aKills: �e"
								+ stats.get("kills"));
						to.sendMessage(StringManager.prefix + " �aDeaths: �e"
								+ stats.get("deaths"));
						to.sendMessage(StringManager.prefix + " �aK/D: �e" + (kd + ""));
						to.sendMessage(StringManager.prefix
								+ "�8-- �eClickIt Stats of �a" + of + " �8--");
					}
				});
	}

}
