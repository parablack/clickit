package net.parablack.ClickIt.command;

import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.servermanager.ServerState;
import me.kombustorlp.gameapi.servermanager.StateValue;
import net.parablack.ClickIt.ClickIt;

import org.bukkit.command.Command;


public class Command_begingame extends PluginCommand {
	public Command_begingame() {
		super("begingame", Rang.DEVELOPER.getLevel());
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
		int wantedTime;
		if (args.length != 1) {
			wantedTime = 10;
		} else {
				wantedTime = Integer.valueOf(args[0]).intValue();
		}
		if(wantedTime > 10) ClickIt.getPluginServer().set(StateValue.SERVERSTATE, ServerState.JOIN.getDatabaseName());
		ClickIt.getInstance().getGameManager().getActualCountdown().setTimeLeft(wantedTime);
	}


}
