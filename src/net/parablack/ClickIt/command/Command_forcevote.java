package net.parablack.ClickIt.command;

import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.ClickIt.ClickIt;
import net.parablack.ClickIt.strings.Message;

import org.bukkit.command.Command;


public class Command_forcevote extends PluginCommand {

	public Command_forcevote() {
		super("forcevote", Rang.DEVELOPER.getLevel());
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
		int forcedmap = 0;
		if(!(args.length == 1)) usageFucker();

		forcedmap = Integer.valueOf(args[0]).intValue();

		Message.p_Forcevote_Map.send(cs.getPlayer(), forcedmap + "");

		ClickIt.getInstance().getMapManager().forcevote(forcedmap);

	}

}
