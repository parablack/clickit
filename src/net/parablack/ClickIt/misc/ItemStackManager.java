package net.parablack.ClickIt.misc;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemStackManager {
	public static ItemStack getCompass(){
		return write(new ItemStack(Material.NETHER_STAR), "�eSpectate a player");
	}
	public static ItemStack getWatch(){
		return write(new ItemStack(Material.WATCH), "�9Start the game", "�4This item let you start the game!", "�cOnly available as a VIP");
	}
	
	
	
	private static ItemStack write(ItemStack raw, String title, String... lore){
		ItemMeta im = raw.getItemMeta();
		im.setDisplayName(title);
		im.setLore(Arrays.asList(lore));
		raw.setItemMeta(im);
		return raw;
	}
}
