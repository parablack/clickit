package net.parablack.API;

import me.kombustorlp.gameapi.misc.User;

import org.bukkit.command.CommandSender;

public interface PluginCommandSender {
	/**
	 * 
	 * @return The Bukkit {@link CommandSender} object which contains default methods
	 */
	public CommandSender getSender();
	
	/**
	 * Gets the Power of this Command-Sender, based on global MySQL-Tables. 
	 * @return The power of the CommandSender, which can be used for permissions
	 */
	public int getPower();
	
	/**
	 * Overrides the method of Bukkit.
	 * @see CommandSender#sendMessage(String)
	 * @param message
	 */
	public void sendMessage(String message);
	
	public User getUser();
}
