package net.parablack.API.command;


public class CommandUtils {

	/**
	 * Builds a string from an Array
	 * @param arr The array with contains the Strings
	 * @param beginIndex The index of the array where the String-Build is starting
	 * @param seperator The seperator which will be between 2 Strings in arr
	 * @return The builded String
	 */
	public static String arrayString(String[] arr, int beginIndex, String seperator){
		String toReturn = "";
		for(int i = beginIndex; i < arr.length; i++){
			toReturn += arr[i] + seperator;
		}
		return toReturn.length() >= seperator.length() ? toReturn.substring(0, toReturn.length() - seperator.length()) : toReturn;
	}
}
