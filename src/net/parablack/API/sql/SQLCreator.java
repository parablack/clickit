package net.parablack.API.sql;

import java.io.InputStream;
import java.io.InputStreamReader;

import me.kombustorlp.gameapi.mysql.MySQL;
import net.parablack.API.PluginClass;


public class SQLCreator {
	public static void createSQL() {
		try {
			InputStream stream = PluginClass.getInstance().getResource("create.sql");
			ScriptRunner run = new ScriptRunner(MySQL.connection, false, false);
			System.out.println(stream);
			InputStreamReader read = new InputStreamReader(stream);
			run.runScript(read);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
