package net.parablack.API.sql;

import java.sql.ResultSet;

import me.kombustorlp.gameapi.mysql.MySQL;


public class CurrentSQL {
	
	public static void update(String update, String... values) {
		MySQL.update(update, values);
	}

	public static ResultSet query(String query, String... values) {
		return MySQL.query(query, values);
	}
}
