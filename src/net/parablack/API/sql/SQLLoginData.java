package net.parablack.API.sql;

import lombok.Getter;
import lombok.Setter;


public class SQLLoginData {
	@Getter @Setter private String username; 
	@Getter @Setter private String host; 
	@Getter @Setter private String password;
	@Getter @Setter private String port; 
	@Getter @Setter private String database; 
}	
