package net.parablack.API;

import net.parablack.API.api.IStringManager;

import org.bukkit.plugin.java.JavaPlugin;

public abstract class PluginClass extends JavaPlugin{
	
	private static JavaPlugin instance;
	
	@Override
	public void onEnable() {
		instance = this;

		
		pluginClassEnable();
	}
	@Override
	public void onDisable() {
		pluginClassDisable();
	}
	@Override
	public void onLoad() {
		pluginClassLoad();
	}

	public void pluginClassDisable() {}

	public void pluginClassEnable() {}

	public void pluginClassLoad() {}

	
	public static JavaPlugin getInstance() {
		return instance;
	}
	public IStringManager getStringManager() {
		return null;
	}
}
