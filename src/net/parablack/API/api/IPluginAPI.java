package net.parablack.API.api;

import java.util.Collection;

import net.parablack.API.PluginCommandSender;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Deprecated
public interface IPluginAPI {
	public Collection<APIPlayer> getOnline();

	public APIPlayer from(Player p);

	void unregister(Player p);

	void registerPlayer(Player p);

	public PluginCommandSender from(CommandSender cs);
}
