package net.parablack.API.api;

import net.parablack.API.PluginCommandSender;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

@Deprecated
public interface APIPlayer extends PluginCommandSender, OfflinePlayer {
	
	public void setName();
	
	public Player getPlayer();
	
}
