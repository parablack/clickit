package net.parablack.API.api;

import java.util.ArrayList;

import net.parablack.ClickIt.api.PluginPlayer;

public class SpectatorAPI {

	private static ArrayList<PluginPlayer> spectators = new ArrayList<>();
	private static ArrayList<PluginPlayer> alive = new ArrayList<>();
	
	public static ArrayList<PluginPlayer> getSpectators() {
		return spectators;
	}
	
	public static ArrayList<PluginPlayer> getAlive() {
		return alive;
	}
	
	public static void unregister(PluginPlayer pl){
		if(alive.contains(pl)) alive.remove(pl);
		if(spectators.contains(pl)) spectators.remove(pl);
	}
	
	
	
}
