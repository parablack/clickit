package net.parablack.API.event;

import net.parablack.ClickIt.api.PluginPlayer;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;
/**
 * 
 * @author parablack
 * 
 * @param <T> The Event which shall be listened for
 */
public abstract class GameEvent <T extends Event> implements Listener{
	
	public GameEvent() {
		
//		Bukkit.getPluginManager().registerEvents(this, MiniGame.getInstance());
	
	}
	
//	@EventHandler(priority = EventPriority.NORMAL )
//	public void fired(T e){
//		
//		if(this.getClass().isAnnotationPresent(NonNullEvent.class)){
//			NonNullEvent a = getClass().getAnnotation(NonNullEvent.class);
//			String[] checkMethods = a.methods();
//			for(String method : checkMethods){
//				try {
//					if(e.getClass().getMethod(method).invoke(e) == null) return;
//				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
//					e1.printStackTrace();
//				}
//			}
//			
//		}
//		
//		// Debug Features maybe here...
//		onEvent(e);
//	}
	/**
	 * 
	 * @param e Event on which shall get listened
	 * 
	 * <b>Annotate this with <code>@EventHandler</code></b>
	 * 
	 */
	public abstract void onEvent(T e);
//	/**
//	 * 
//	 * @return The actual {@link GameState}
//	 */
//	protected GameState gameState(){
//		return MiniGame.getInstance().getGameManager().getGameState();
//	}
	/**
	 * 
	 * @param p Player from which the object should come from
	 * @return The fitting {@link IPluginPlayer} Object from p
	 */
	protected PluginPlayer from(Player p){
		return SC.from(p);
	}
	
}
