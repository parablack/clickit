package net.parablack.MiniGame.api.map;

public interface IMapInformation {
	
	public int getVotableMaps();
	
	public String getTableName();
	
}
