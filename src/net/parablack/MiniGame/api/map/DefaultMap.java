package net.parablack.MiniGame.api.map;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.parablack.API.sql.CurrentSQL;
import net.parablack.MiniGame.api.MiniGame;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;


public class DefaultMap implements IMap {

	private int id;
	private Location spawn;
	private Location specSpawn;
	private String displayName;
	private String worldname;
	private String creator;
	private String link;
	private String meta;
	private boolean listed;
	private int votes = 0;

	public DefaultMap(int id) {
		this.id = id;
		final ResultSet rs = CurrentSQL.query("SELECT * FROM " + MiniGame.getInstance().getMapManager().getTableName() + " WHERE id=?", id + "");
	//	System.out.println("xyz");
		try {
	//		System.out.println("nfe");
			rs.first();
			worldname = rs.getString("world");
//			System.out.println("sad");
			if(Bukkit.getWorld(worldname) == null){
//				System.out.println(worldname + " #xrat");
//				Bukkit.getScheduler().scheduleSyncDelayedTask(MiniGame.getInstance(), new Runnable() {
//
//				@Override
//				public void run() {
//					System.out.println("y");
					Bukkit.createWorld(new WorldCreator(worldname));
					World w = Bukkit.getWorld(worldname);
//					try {
						spawn = new Location(w, rs.getLong("spawn_x"), rs.getLong("spawn_y"), rs.getLong("spawn_z"));
//					} catch (SQLException e1) {
//						e1.printStackTrace();
//					}
//					try {
						specSpawn = new Location(w, rs.getLong("specspawn_x"), rs.getLong("specspawn_y"), rs.getLong("specspawn_z"));
//					} catch (SQLException e) {
//						e.printStackTrace();
//					}
//				}
//			});
		}
			else {
		//		System.out.println("asda");
				World w = Bukkit.getWorld(worldname);
	
					spawn = new Location(w, rs.getLong("spawn_x"), rs.getLong("spawn_y"), rs.getLong("spawn_z"));
					System.out.println("x");
			
					specSpawn = new Location(w, rs.getLong("specspawn_x"), rs.getLong("specspawn_y"), rs.getLong("specspawn_z"));
				
			}
	//		System.out.println(Bukkit.getWorld(worldname));
	//		System.out.println(spawn);
			

			displayName = rs.getString("display_name");
			creator = rs.getString("creator");
			link = rs.getString("link");
			listed = rs.getString("listed").equals("true");
			link = rs.getString("link");
			meta = rs.getString("meta");

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Location getSpawn() {
		return spawn;
	}

	@Override
	public Location getSpectatorSpawn() {
		return specSpawn;
	}

	@Override
	public String getName() {
		return displayName;
	}

	@Override
	public String getCreator() {
		return creator;
	}

	@Override
	public String getLink() {
		return link;
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public String getWorldName() {
		return worldname;
	}

	@Override
	public String getMeta() {
		return meta;
	}

	@Override
	public boolean isListed() {
		return listed;
	}

	@Override
	public int getVotes() {
		return votes;
	}

	public void vote() {
		votes++;
	}

}
