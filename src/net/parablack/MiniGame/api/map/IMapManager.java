package net.parablack.MiniGame.api.map;

import java.util.ArrayList;

import net.parablack.ClickIt.exception.MapNotChoosenException;
import net.parablack.MiniGame.api.IMiniGame;

public interface IMapManager {
	/**
	 * The ID of the Lobby Map. Should be used in every {@link IMiniGame}
	 */
	public static int C_LOBBY_UUID = -1;
	
	
	public void setMaps(ArrayList<IMap> maps);
	
	public IMap getLobby();
	
	/**
	 * Chooses the map which will be played.
	 * Order: 1. Forcevoted Map
	 * 		  2. Map with most votes
	 * 		  3. One random Map with the most votes
	 * 
	 */
	public void choose();
	
	public boolean isVoteEnabled();
	
	public void setVoteEnabled(boolean enabled);
	
	public boolean hasVoted(String uuid);
	
	public void setVoted(String uuid);
	
	public IMap getPlayedMap() throws MapNotChoosenException;
	
	public int getForcevoted();
	
	//public void setPlayedMap(IMap map);
	
	/**
	 * 
	 * @param vote The Map which should be forcevoted
	 */
	public void forcevote(int vote);
	
	/**
	 * Returns an array of votable Maps, which could get displayed to the user.
	 * @return {@link IMap} Array containing votable maps, in order of [0 - (Max Votable Maps)]
 	 */
	public ArrayList<IMap> getVotableMaps();
	
	public String getTableName();
	
	public int getMapCount();

	public ArrayList<IMap> getMaps();
		
	public void load();


}
