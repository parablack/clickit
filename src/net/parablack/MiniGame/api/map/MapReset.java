package net.parablack.MiniGame.api.map;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.parablack.MiniGame.api.MiniGame;

import org.bukkit.Bukkit;

public class MapReset {
	public static void deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (String element : children) {
				deleteDir(new File(dir, element));
			}
		}
		dir.delete();
	}

	public static void resetMap() {
		if(MiniGame.getInstance().getMapManager().getPlayedMap() == null){
			System.err.println("No map found! Whatsup here? Aborting...");
			return;
		}
		String map1name = MiniGame.getInstance().getMapManager().getPlayedMap().getSpawn().getWorld().getName();
		
		try {
			Bukkit.getServer().unloadWorld(map1name, false);
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		deleteDir(new File(map1name));

		File srcFolder1 = new File("../SharedFiles/ClickIt/maps/" + map1name);
		File destFolder1 = new File(map1name);
		try {
			copyFolder(srcFolder1, destFolder1);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void copyFolder(File src, File dest) throws IOException {

		if (src.isDirectory()) {
			if (!dest.exists()) {
				dest.mkdir();
				System.out.println("Directory copied from " + src + "  to " + dest);
			}

			String files[] = src.list();

			for (String file : files) {
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				copyFolder(srcFile, destFile);
			}

		} else {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);

			byte[] buffer = new byte[1024];

			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
		}
	}
}
