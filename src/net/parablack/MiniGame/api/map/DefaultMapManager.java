package net.parablack.MiniGame.api.map;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import net.parablack.API.sql.CurrentSQL;

public class DefaultMapManager implements IMapManager{

	private IMapInformation info;

	private ArrayList<IMap> maps;
	
	private ArrayList<IMap> choosableMaps;
	
	private IMap lobby;
	
	private int forcevoted = Integer.MIN_VALUE;
	
	private boolean voteEnabled = true;
	
	private IMap played;
	
	private ArrayList<String> voted = new ArrayList<>();
	
	public DefaultMapManager(IMapInformation info){
		
		this.info = info;
		
	}
	
	public void load(){
		ArrayList<IMap> t_maps = new ArrayList<>();
		
		ResultSet rs = CurrentSQL.query("SELECT * FROM " + getTableName() + " WHERE listed='true' ORDER BY id");
		
		try {
			while(rs.next()){
				t_maps.add(new DefaultMap(rs.getInt("id")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		setMaps(t_maps);
		loadXX();
	}
	
	private void loadXX(){
		ResultSet rs = CurrentSQL.query("SELECT * FROM " + getTableName() + " WHERE listed='false' ORDER BY id");
		
		try {
			while(rs.next()){
				new DefaultMap(rs.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public void setMaps(ArrayList<IMap> maps) {
		this.maps = maps;
		
		ArrayList<IMap> listed = new ArrayList<>();
		for(IMap i : this.maps) if(i.isListed()) listed.add(i);
		
		if(info.getVotableMaps() >= listed.size()) choosableMaps = listed;
		else{
			choosableMaps = new ArrayList<>();
			for(int i = 0; i < info.getVotableMaps(); i++){
				int rnd = (int) Math.round(((Math.random()) * listed.size()));
				rnd = Math.max(rnd, 0);
				rnd = Math.min(rnd, listed.size() - 1);
				IMap mapp = listed.get(rnd);
				choosableMaps.add(mapp);
				
				listed.remove(rnd);
			}
			
		}
		
		//Select choosable maps
		
	}

	@Override
	public IMap getLobby() {
		return lobby == null ? (lobby = new DefaultMap(C_LOBBY_UUID)) : lobby;
	}

	@Override
	public void choose() {
		IMap highest = null;
		if(forcevoted != Integer.MIN_VALUE){
			this.played = new DefaultMap(forcevoted);
			return;
		}
		for(int i = 0; i < choosableMaps.size(); i++){
			IMap temp_map = choosableMaps.get(i);
			if(highest == null) highest = temp_map;
			if((temp_map.getVotes() == highest.getVotes()) && Math.random() < 0.5) highest = temp_map;
			if(temp_map.getVotes() > highest.getVotes()) highest = temp_map;
		}
	//	Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mv load " + highest.getWorldName());
		this.played = highest;
		//fMiniGame.getInstance().getGameConnector().getServer().setMap(played.getName());
		
	}

	@Override
	public boolean isVoteEnabled() {
		return voteEnabled;
	}

	@Override
	public void setVoteEnabled(boolean enabled) {
		this.voteEnabled = enabled;
	}

	@Override
	public boolean hasVoted(String uuid) {
		return voted.contains(uuid);
	}

	@Override
	public void setVoted(String uuid) {
		voted.add(uuid);
	}

	@Override
	public IMap getPlayedMap(){
		return played;
	}

	@Override
	public int getForcevoted() {
		return forcevoted;
	}

	@Override
	public void forcevote(int vote) {
		this.forcevoted = vote;
	}

	@Override
	public ArrayList<IMap> getVotableMaps() {
		return choosableMaps;
	}

	@Override
	public int getMapCount() {
		ResultSet rs = CurrentSQL.query("SELECT count(*) FROM " + getTableName() + " WHERE listed='true'");
		try {
			rs.first();
			return rs.getInt("count(*)");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public String getTableName() {
		return info.getTableName();
	}
	@Override
	public ArrayList<IMap> getMaps() {
		return maps;
	}
}
