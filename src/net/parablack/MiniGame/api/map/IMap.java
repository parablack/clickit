package net.parablack.MiniGame.api.map;

import org.bukkit.Location;

public interface IMap {
	public Location getSpawn();
	
	public Location getSpectatorSpawn();
	
	public String getName();
	
	public String getCreator();
	
	public String getLink();
	
	public int getID();
	
	public String getWorldName();
	
	public String getMeta();

	public boolean isListed();
	
	public void vote();
	
	public int getVotes();
}
