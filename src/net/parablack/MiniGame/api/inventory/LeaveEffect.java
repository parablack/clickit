package net.parablack.MiniGame.api.inventory;

import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.event.player.PlayerInteractEvent;

public class LeaveEffect implements ClickEffect{

	@Override
	public void click(IPluginPlayer p, PlayerInteractEvent e) {
		p.getPlayer().kickPlayer("You have been returned to hub!");
	}

}
