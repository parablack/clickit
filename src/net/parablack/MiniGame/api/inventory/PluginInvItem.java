package net.parablack.MiniGame.api.inventory;

import org.bukkit.inventory.ItemStack;

public class PluginInvItem {
	private ItemStack stack;
	private int pos;
	private ClickEffect e;
	private String perm;
	
	public PluginInvItem(ItemStack stack, int pos, ClickEffect e) {
		this.setStack(stack);
		this.setPos(pos);
		this.setE(e);
		perm = null;
	}
	public PluginInvItem(ItemStack stack, int pos, ClickEffect e, String perm) {
		this.setStack(stack);
		this.setPos(pos);
		this.setE(e);
		this.perm = perm;
	}

	public ItemStack getStack() {
		return stack;
	}

	public void setStack(ItemStack stack) {
		this.stack = stack;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public ClickEffect getE() {
		return e;
	}

	public void setE(ClickEffect e) {
		this.e = e;
	}
	public String getPerm() {
		return perm;
	}
	public void setPerm(String perm) {
		this.perm = perm;
	}
}
