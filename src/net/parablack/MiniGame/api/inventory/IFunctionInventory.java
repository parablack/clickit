package net.parablack.MiniGame.api.inventory;

import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.event.player.PlayerInteractEvent;

public interface IFunctionInventory {
	public void add(IPluginPlayer p); 
	
	public void addItem(PluginInvItem i);

	public void fire(PlayerInteractEvent e);
	
}
