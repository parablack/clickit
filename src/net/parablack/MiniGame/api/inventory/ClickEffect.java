package net.parablack.MiniGame.api.inventory;

import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.event.player.PlayerInteractEvent;

public interface ClickEffect {
	public void click(IPluginPlayer p, PlayerInteractEvent e);
}
