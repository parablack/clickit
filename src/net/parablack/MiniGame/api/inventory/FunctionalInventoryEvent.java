package net.parablack.MiniGame.api.inventory;

import net.parablack.API.event.GameEvent;
import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.MiniGame;
import net.parablack.MiniGame.api.game.IGameManager.GameState;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class FunctionalInventoryEvent extends GameEvent<PlayerInteractEvent>{
	
	@EventHandler
	@Override
	public void onEvent(PlayerInteractEvent e) 
	{
	
		if(!(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
		if(e.getItem() == null) return;
		if(e.getMaterial() == null) return;
		
		if(SC.gameState() == GameState.LOBBY){
//			System.out.println(MiniGame.getInstance());
//			System.out.println(MiniGame.getInstance().getLobbyInventory());
//			MiniGame.getInstance().getLobbyInventory().fire(e);
			return;
		}
		if(!(SC.from(e.getPlayer())).isSpectating()) return;
		
		MiniGame.getInstance().getSpectatorInventory().fire(e);
		
	}

}
