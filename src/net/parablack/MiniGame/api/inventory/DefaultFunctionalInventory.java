package net.parablack.MiniGame.api.inventory;

import java.util.ArrayList;
import java.util.HashMap;

import net.parablack.ClickIt.utils.SC;
import net.parablack.MiniGame.api.player.IPluginPlayer;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;

public class DefaultFunctionalInventory implements IFunctionInventory {

	private ArrayList<PluginInvItem> items = new ArrayList<>();
	private HashMap<Material, ArrayList<ClickEffect>> effects = new HashMap<>();
	
	
	@Override
	public void add(IPluginPlayer p) {
		for(PluginInvItem i : items){
			if(i.getPerm() == null || p.getPlayer().hasPermission(i.getPerm())) p.getPlayer().getInventory().setItem(i.getPos(), i.getStack());
		}
		
		
	}

	@Override
	public void addItem(PluginInvItem i) {
		
		items.add(i);
		if(!effects.containsKey(i.getStack().getType())){
			ArrayList<ClickEffect> arr = new ArrayList<>();
			arr.add(i.getE());
			effects.put(i.getStack().getType(), arr);
		}else effects.get(i.getStack().getType()).add(i.getE());
		
	}

	@Override
	public void fire(PlayerInteractEvent e) {
		if(effects.containsKey(e.getItem().getType())){
			for(ClickEffect eff : effects.get(e.getItem().getType())) eff.click(SC.from(e.getPlayer()), e);	
		}
	}

}
