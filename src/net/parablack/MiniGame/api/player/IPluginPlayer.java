package net.parablack.MiniGame.api.player;

import org.bukkit.entity.Player;


public interface IPluginPlayer {
	
	public boolean isSpectating();

	public void setSpectator();
	
	public Player getPlayer();



	
}
