package net.parablack.MiniGame.api.player;

public interface IBasicStatistics {
	public int getKills();

	public int getPoints();

	public int getDeaths();

	public int getWins();

	public int getGamesPlayed();

	public void setKills(int kills);

	public void addKill();

	public void setPoints(int points);

	public void setDeaths(int deaths);

	public void addDeath();

	public void setWins(int wins);

	public void addWin();

	public void setGamesPlayed(int gamesplayed);

	public void addGamePlayed();

	public void addPoints(int points);

}
