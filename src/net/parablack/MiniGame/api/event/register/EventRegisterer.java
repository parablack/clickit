package net.parablack.MiniGame.api.event.register;

import net.minecraft.util.com.google.common.reflect.ClassPath;
import net.parablack.API.event.GameEvent;


public class EventRegisterer {
	
	private String path;
	public EventRegisterer(String absolutePath) {
		this.path = absolutePath;
		
		
	}
	
	public void register(){
		try {
			for (ClassPath.ClassInfo clazz : ClassPath.from(getClass().getClassLoader()).getTopLevelClasses(path)) {
				Class<?> c = Class.forName(clazz.getName());
				if(!c.isAnnotationPresent(NoEvent.class)) {
					Object instance = c.newInstance();
					if(!(instance instanceof GameEvent)){
			//			MiniGame.getInstance().getPluginLogger().logAdmin("Non-GameEvent class founded in absolute Listener path! This should be fixed by annotating the class with '@NoEvent'");
						continue;
					}
					
					// Event gets automatically registered

					
				}else{
					System.out.println("Found non-event class: " + clazz.getSimpleName());
				}
			}
		} catch (Exception ex) {
			System.err.println("Error while registering events!");
			ex.printStackTrace();
		}
	}
	
}
