package net.parablack.MiniGame.api;

import me.kombustorlp.gameapi.servermanager.ServerState;
import me.kombustorlp.gameapi.servermanager.StateValue;
import net.parablack.API.PluginClass;
import net.parablack.API.sql.SQLCreator;
import net.parablack.ClickIt.ClickIt;
import net.parablack.MiniGame.api.command.defaults.Command_addmap;
import net.parablack.MiniGame.api.command.defaults.Command_minigame;
import net.parablack.MiniGame.api.command.defaults.Command_setspawn;
import net.parablack.MiniGame.api.command.defaults.Command_worldtp;
import net.parablack.MiniGame.api.inventory.FunctionalInventoryEvent;

import org.bukkit.Bukkit;

public abstract class MiniGame extends PluginClass implements IMiniGame{

	private static MiniGame thisinstance;
	
	//private IServerManager servermg = new MiniGameServerManager();
	
	@Override
	public void pluginClassEnable(){
		thisinstance = this;
		
		SQLCreator.createSQL();
		new Command_setspawn();
		new Command_worldtp();
		new Command_addmap();
		new Command_minigame();
		
	
		
		pluginEnable();
		getMapManager().load();
	//	Server s = getGameConnector().getServer();
		
	//	s.setState(ServerState.);
	//	s.setPlayers(0);
	//	s.setDetailstate("Starting");
	//	s.setMap("None");
		
		ClickIt.getPluginServer().set(StateValue.GAMESTATE, "Lobby");
		ClickIt.getPluginServer().set(StateValue.SERVERSTATE, ServerState.JOIN.getDatabaseName());
		ClickIt.getPluginServer().set(StateValue.MAXPLAYERS, "16");
		ClickIt.getPluginServer().set(StateValue.PLAYERSONLINE, "0");
		
		Bukkit.getPluginManager().registerEvents(new FunctionalInventoryEvent(), this);

		
	
		
	}
	@Override
	public void pluginClassDisable(){
		pluginDisable();
	}
	@Override
	public void pluginClassLoad(){
		pluginLoad();
	}
	
	public abstract void pluginEnable();
	
	public abstract void pluginDisable();
		
	public abstract void pluginLoad();
	
	public static MiniGame getInstance(){
		return thisinstance;
	}
	public abstract MiniGame getGame();
}
