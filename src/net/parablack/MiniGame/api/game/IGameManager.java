package net.parablack.MiniGame.api.game;

import me.kombustorlp.gameapi.servermanager.ServerState;


public interface IGameManager {
	
	public GameState getGameState();
	
	
	
	public enum GameState{
		INIT(ServerState.RESTARTING),
		LOBBY(ServerState.JOIN),
		WARMUP(ServerState.NOTJOINABLE),
		GRACE(ServerState.NOTJOINABLE),
		INGAME(ServerState.JOIN_INGAME),
		AFTER_GAME(ServerState.NOTJOINABLE),
		DEATHMATCH(ServerState.NOTJOINABLE),
		CUSTOM(ServerState.NOTJOINABLE),
		RESTART(ServerState.RESTARTING),
		OFFLINE(ServerState.OFFLINE);
	
		private ServerState state;
		
		private GameState(ServerState s) {
			state = s;
		}
		
		public ServerState getServerState(){
			return state;
		}
	}



	public void setActualCountdown(Countdown countdown);
	
	public Countdown getActualCountdown();

	public void setGameState(GameState gameState);
	
}
