package net.parablack.MiniGame.api.game;

import java.util.Timer;
import java.util.TimerTask;

import net.parablack.ClickIt.utils.StateType;
import net.parablack.MiniGame.api.MiniGame;

public abstract class Countdown implements StateType{
	
	private int time;
	private int timeIntervall = 1000;
	Timer startTimer = new Timer();
	
	public Countdown(int time, int timeIntervall) {
		this.time = time;
	}
	public Countdown(int time) {
		this(time, 1000);
	}
	
	public void start(){
		MiniGame.getInstance().getGameManager().setActualCountdown(this);
		MiniGame.getInstance().getGameManager().setGameState(getGameState());
		
		initialize();
		TimerTask countdownTimer = new TimerTask() {

			@Override
			public void run() {
				if (time <= 0) {
					startTimer.cancel();
					end();
					return;
				}
				next(time);
				time--;
			}
		};
		startTimer.schedule(countdownTimer, timeIntervall, timeIntervall);
	}
	
	public void stop(){
		startTimer.cancel();
	}
	
	public int getTimeLeft(){
		return this.time;
	}
	public void setTimeLeft(int time){
		this.time = time;
	}
	
	public abstract void initialize();
	public abstract void next(int leftTime);
	public abstract void end();
}
