package net.parablack.MiniGame.api.command.defaults;

import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.API.command.CommandUtils;
import net.parablack.MiniGame.api.MiniGame;
import net.parablack.MiniGame.api.map.IMap;

import org.bukkit.command.Command;


public class Command_minigame extends PluginCommand {

	public Command_minigame() {
		super("minigame", Rang.DEVELOPER.getLevel());
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {

		if(args.length == 0) {
			cs.sendMessage(MiniGame.getInstance().getStringManager().getPrefix() + "�a" + MiniGame.getInstance().getName() + " �av." + MiniGame.getInstance().getDescription().getVersion() + " �6by �4" + CommandUtils.arrayString(MiniGame.getInstance().getDescription().getAuthors().toArray(new String[MiniGame.getInstance().getDescription().getAuthors().size()]), 0, ", "));
			cs.sendMessage(MiniGame.getInstance().getStringManager().getPrefix() + "�4This plugin is using the MiniGameAPI by �aparablack");
			if(cs.getRang().getLevel() >= Rang.DEVELOPER.getLevel()) {
				cs.sendMessage(MiniGame.getInstance().getStringManager().getPrefix() + "�aType �e�o/minigame help �afor more commands");
			}
			return;
		}

		if(args.length == 1 && cs.getRang().getLevel() >= Rang.DEVELOPER.getLevel()) {

			switch (args[0]) {
			case "help":

				cs.sendMessage("�4MiniGame help menu: ");
				cs.sendMessage("�e>�a /minigame maps �6: Map-Editing Menu");
				cs.sendMessage("�e>�a /minigame server �6: Server-Editing Menu");
				break;

			case "maps":

				for (IMap i : MiniGame.getInstance().getMapManager().getMaps()) {

					cs.sendMessage(i.getID() + ": " + i.getName() + "�r (" + i.getCreator() + ", " + i.getLink() + ") (Votes: " + i.getVotes() + ") in " + i.getWorldName() + " | " + (i.isListed() ? "�aLISTED"
							: "�4UNLISTED"));
				}

				break;
			case "server":

				break;
			default:
				break;
			}

		}

	}

}
