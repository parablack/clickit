package net.parablack.MiniGame.api.command.defaults;

import me.kombustorlp.gameapi.cmds.utils.Descriptable;
import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.API.command.CommandUtils;
import net.parablack.API.sql.CurrentSQL;
import net.parablack.MiniGame.api.MiniGame;

import org.bukkit.command.Command;


@Descriptable(usage = "<ID> <Listed[true:false]> <Displayname>")
public class Command_addmap extends PluginCommand {

	public Command_addmap() {
		super("addmap", Rang.ADMIN.getLevel());
	}



	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
		if(args.length==0) usageFucker();
		
		int id = Integer.parseInt(args[0]);
		boolean listed = args[1].equalsIgnoreCase("true");
		
		String disp = CommandUtils.arrayString(args, 2, " ");
		
		CurrentSQL.update("INSERT INTO " + MiniGame.getInstance().getMapManager().getTableName() + " (id, listed, display_name, world) VALUES (?, ?, ?, ?)", id+"", listed+"", disp, cs.getPlayer().getWorld().getName());
		
		cs.sendMessage(MiniGame.getInstance().getStringManager().getPrefix() + "�aSuccessfuly created the Map �c#" + id + " �awith the name '�c"+disp+"�a'");
		cs.sendMessage(MiniGame.getInstance().getStringManager().getPrefix() + "�aThis Map is " + (listed ? "�cLISTED" : "�cNOT LISTED" ));
			
	}
}