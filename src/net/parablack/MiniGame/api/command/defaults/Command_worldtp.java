package net.parablack.MiniGame.api.command.defaults;

import me.kombustorlp.gameapi.cmds.utils.Descriptable;
import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.MiniGame.api.MiniGame;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

@Descriptable(usage = "<World>")
public class Command_worldtp extends PluginCommand{

	public Command_worldtp() {
		super("worldtp", Rang.DEVELOPER.getLevel());
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
		if(args.length != 1) usageFucker();
		
		Player p = cs.getPlayer();
		
		String world = args[0];
		
		if(Bukkit.getWorld(world) == null){
			Bukkit.createWorld(new WorldCreator(world));
		}
		World w = Bukkit.getWorld(world);
		
		p.teleport(new Location(w, p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ()));
		
		p.sendMessage(MiniGame.getInstance().getStringManager().getPrefix() + "�aTeleported to the specified world '�c"+world+"�a'");
		
	}

}
