package net.parablack.MiniGame.api.command.defaults;

import me.kombustorlp.gameapi.cmds.utils.PluginCommand;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import net.parablack.API.sql.CurrentSQL;
import net.parablack.MiniGame.api.MiniGame;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;


public class Command_setspawn extends PluginCommand {

	public Command_setspawn() {
		super("setspawn", Rang.DEVELOPER.getLevel());
	}

	@Override
	public void execute(User cs, Command c, String[] args) throws Exception {
		if(args.length < 1) usageFucker();

		Player p = cs.getPlayer();
		Location loc = p.getLocation();

		int mapID = Integer.parseInt(args[0]);

		boolean spectator = args.length == 2 && (args[1].equals("spectator") || args[1].equals("spec"));

		double x = loc.getX();
		double y = loc.getY();
		double z = loc.getZ();

		if(spectator) {
			CurrentSQL.update("UPDATE " + MiniGame.getInstance().getMapManager().getTableName() + " SET specspawn_x=?, specspawn_y=?, specspawn_z=?, world=? WHERE id=?", x + "", y + "", z + "", loc.getWorld().getName(), mapID + "");
			cs.sendMessage("�aSpectator-Spawn set!");
		} else {
			CurrentSQL.update("UPDATE " + MiniGame.getInstance().getMapManager().getTableName() + " SET spawn_x=?, spawn_y=?, spawn_z=?, world=? WHERE id=?", x + "", y + "", z + "", loc.getWorld().getName(), mapID + "");
			cs.sendMessage("�aSpawn set");
		}

	}

}
