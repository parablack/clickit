package net.parablack.MiniGame.api;

import java.util.ArrayList;

import net.parablack.API.api.IPluginAPI;
import net.parablack.MiniGame.api.player.IPluginPlayer;
@Deprecated
public interface IGameAPI extends IPluginAPI{
	
	
	public ArrayList<IPluginPlayer> getSpectators();
	
	public ArrayList<IPluginPlayer> getAlive();

	
}
