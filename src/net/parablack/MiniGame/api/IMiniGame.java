package net.parablack.MiniGame.api;

import net.parablack.MiniGame.api.game.IGameManager;
import net.parablack.MiniGame.api.inventory.IFunctionInventory;
import net.parablack.MiniGame.api.map.IMapManager;

public interface IMiniGame {
	public IMapManager getMapManager();
	
	public String getName();
	
	public IGameManager getGameManager();

	public IFunctionInventory getSpectatorInventory();
	
}
